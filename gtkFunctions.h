// gtkFunctions.h
//
// GTK-based graphical interface for epr123
//

#include <vector>

#include "eprTypes.h"
#include "eprInstrument.h"

#define WIDTH 1024
#define HEIGHT 768
#define FONT_SZ 16

void gTuningMode(eprInstrument &epr,
		 const EPR::settings &sets,
		 EPR::status_t &eprStatus);

void gShowData(std::vector<double> &rawData, EPR::settings &sets);
