# epr123 makefile
#

DEBUG := -Wall
LIBS := -larchive \
	-lreadline \
	-pthread \
	$(shell pkg-config --libs gtk+-3.0)

NIMHDDK_FLAGS := -DkLittleEndian=1 -DkGNU=1

NIMHDDK := NIMHDDK/nimhddk_linuxkernel
NIXSERIES := NIMHDDK/nixseries

INCLUDES := -I$(NIMHDDK) \
	-I$(NIXSERIES) \
	-I$(NIXSERIES)/ChipObjects \
	-I$(NIXSERIES)/Examples \
	$(shell pkg-config --cflags gtk+-3.0)

NIOBJECTS := $(NIXSERIES)/osiBus.o \
	$(NIXSERIES)/osiUserCode.o \
	\
	$(NIXSERIES)/tCHInChDMAChannelController.o \
	$(NIXSERIES)/tCHInChDMAChannel.o \
	$(NIXSERIES)/tCHInChSGLChunkyLink.o \
	$(NIXSERIES)/tCHInChSGL.o \
	$(NIXSERIES)/tDMABuffer.o \
	$(NIXSERIES)/tLinearDMABuffer.o \
	$(NIXSERIES)/tScatterGatherDMABuffer.o \
	\
	$(NIXSERIES)/tAI.o \
	$(NIXSERIES)/tAO.o \
	$(NIXSERIES)/tBrdServices.o \
	$(NIXSERIES)/tBusInterface.o \
	$(NIXSERIES)/tCHInCh.o \
	$(NIXSERIES)/tCounter.o \
	$(NIXSERIES)/tDI.o \
	$(NIXSERIES)/tDMAController.o \
	$(NIXSERIES)/tDO.o \
	$(NIXSERIES)/tInTimer.o \
	$(NIXSERIES)/tOutTimer.o \
	$(NIXSERIES)/tSimultaneousControl.o \
	$(NIXSERIES)/tStreamCircuitRegMap.o \
	$(NIXSERIES)/tTriggers.o \
	$(NIXSERIES)/tXSeries.o \
	\
	$(NIXSERIES)/aiHelper.o \
	$(NIXSERIES)/aoHelper.o \
	$(NIXSERIES)/dataHelper.o \
	$(NIXSERIES)/devices.o \
	$(NIXSERIES)/eepromHelper.o \
	$(NIXSERIES)/inTimerHelper.o \
	$(NIXSERIES)/inTimerParams.o \
	$(NIXSERIES)/outTimerHelper.o \
	$(NIXSERIES)/simultaneousInit.o \
	$(NIXSERIES)/streamHelper.o

HEADERS := eprTypes.h \
	supplement.h \
	fileOps.h \
	fieldSweep.h \
	gradients.h \
	eprInstrument.h \
	frqCounter.h \
	gtkFunctions.h

SOURCES := epr123.cpp \
	supplement.cpp \
	fileOps.cpp \
	fieldSweep.cpp \
	gradients.cpp \
	eprInstrument.cpp \
	frqCounter.cpp \
	gtkFunctions.cpp

OBJDIR := objects

OBJECTS := $(patsubst %.cpp, $(OBJDIR)/%.o, $(SOURCES))

.PHONY: all
all: prepare $(OBJECTS) epr123

.PHONY: prepare
prepare:
	@mkdir -p $(OBJDIR)

$(OBJDIR)/%.o: %.cpp $(HEADERS)
	g++ -c $(DEBUG) $(NIMHDDK_FLAGS) $(INCLUDES) $< -o $@

epr123: $(OBJECTS)
	g++ $(DEBUG) $(NIOBJECTS) $(OBJECTS) $(LIBS) -o $@

.PHONY: install
install:
	@install -vm 755 epr123 /usr/local/bin/epr123
	@install -vm 755 mksweep /usr/local/bin/mksweep
	@install -vm 644 -t /usr/local/etc \
	       etc/daq.devices \
	       etc/default.exp \
	       etc/epr.constants \
	       etc/frq.counter
	@install -vd /usr/local/etc/sw_calib.d
	@install -vm 644 etc/sw_calib.d/* /usr/local/etc/sw_calib.d/

.PHONY: uninstall
uninstall:
	@rm -vf /usr/local/bin/epr123
	@rm -vf /usr/local/bin/mksweep
	@rm -vf /usr/local/etc/daq.devices \
		/usr/local/etc/default.exp \
		/usr/local/etc/epr.constants \
		/usr/local/etc/frq.counter
	@rm -vrf /usr/local/etc/sw_calib.d

.PHONY: clean
clean:
	@rm -vrf epr123 $(OBJDIR)
