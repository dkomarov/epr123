// supplement.h
//
// Supplementary functions for epr123 program
//

#include "eprTypes.h"

int getEPRCalibrations(EPR::calibrations &cals);

int loadImagingSettings(FILE *file, EPR::settings &sets);

void setDefaults(EPR::settings &sets);

void auxiliarySettings(EPR::settings &sets, EPR::calibrations cals);

void interpretCenterField(char *str, EPR::settings &sets);

void interpretGradients(char *str, EPR::settings &sets);

void mergeProjections(std::vector<double> &prj0,
		      std::vector<float> &rawData,
		      EPR::settings &sets);

void printHelp();

void printSets(EPR::settings &sets);




//	void flipEvenScans(std::vector<float> &data, size_t n);


/*












unsigned int chkGradients(std::vector<double> &xyzGauss1cm, double *grGauss1cmMax);

unsigned int chkSamplingRate(size_t n_, double swTime, double sampleRateMax);

unsigned int chkEPRsettings(std::vector<double> &swDataGauss,
		std::vector<double> &xyzGauss1cm,
		EPR::settings &sets, EPR::calibrations cals);


*/
