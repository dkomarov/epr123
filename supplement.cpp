// supplement.cpp
//
// Supplementary functions for epr123 program
//

#include <cmath>
#include <cstdio>
#include <cstring>

#include <algorithm>
#include <functional>

#include "eprTypes.h"
#include "supplement.h"

//==============================================================================
#define CALIB_ALL 0b00000000000011001101101100110110

int getEPRCalibrations(EPR::calibrations &cals)
{
  FILE *file;
  char line[MAX_LENGTH];
  unsigned int chkCounter = 0;

  file = fopen(EPR_CONSTANTS, "r");
  if (file == NULL) {
    return 0;
  }

  while (fgets(line, MAX_LENGTH, file)) {
    if (*line == '#' || *line == '\n') {
      continue;
    }
    else if (sscanf(line, "MAIN_FIELD = %lf", &cals.cfGauss1V) == 1) {
      chkCounter |= 1 << 1;
    }
    else if (sscanf(line, "MAXIMUM_FIELD = %lf", &cals.cfGaussMax) == 1) {
      chkCounter |= 1 << 2;
    }
    else if (sscanf(line, "SWEEP_COIL = %lf", &cals.swGauss1V) == 1) {
      chkCounter |= 1 << 4;
    }
    else if (sscanf(line, "MAXIMUM_SWEEP = %lf", &cals.swGaussMax) == 1) {
      chkCounter |= 1 << 5;
    }
    else if (sscanf(line, "X_GRADIENT = %lf", cals.grGauss1cm1V) == 1) {
      chkCounter |= 1 << 8;
    }
    else if (sscanf(line, "MAXIMUM_X_GRADIENT = %lf",
		    cals.grGauss1cmMax) == 1) {
      chkCounter |= 1 << 9;
    }
    else if (sscanf(line, "Y_GRADIENT = %lf", cals.grGauss1cm1V + 1) == 1) {
      chkCounter |= 1 << 11;
    }
    else if (sscanf(line, "MAXIMUM_Y_GRADIENT = %lf",
		    cals.grGauss1cmMax + 1) == 1) {
      chkCounter |= 1 << 12;
    }
    else if (sscanf(line, "Z_GRADIENT = %lf", cals.grGauss1cm1V + 2) == 1) {
      chkCounter |= 1 << 14;
    }
    else if (sscanf(line, "MAXIMUM_Z_GRADIENT = %lf",
		    cals.grGauss1cmMax + 2) == 1) {
      chkCounter |= 1 << 15;
    }
    else if (sscanf(line, "SWEEP_OVERHEAD = %lf", &cals.swOverhead) == 1) {
      chkCounter |= 1 << 18;
    }
    else if (sscanf(line, "SYNC_DELAY = %lf", &cals.syncDelay) == 1) {
      chkCounter |= 1 << 19;
    }
  }

  fclose(file);
  return (chkCounter == CALIB_ALL);
}

//==============================================================================
#define SETS_ALL 0b00000000000001011010011001010110

int loadImagingSettings(FILE *file, EPR::settings &sets)
{
  char line[MAX_LENGTH];
  char str[MAX_LENGTH];
  double dbl;
  unsigned int chkCounter = 0;

  if (file == NULL) {
    return 0;
  }

  while (fgets(line, MAX_LENGTH, file)) {
    if (*line == '#' || *line == '\n') {
      continue;
    }
    else if (sscanf(line, "CENTER_FIELD = %lf", &dbl) == 1) {
      sets.cfAuto = 0;
      sets.cfShift = 0.0;
      sets.cfGauss = dbl;
      chkCounter |= 1 << 1;
    }
    else if (sscanf(line, "CENTER_FIELD = auto + %lf", &dbl) == 1) {
      sets.cfAuto = 1;
      sets.cfShift = dbl;
      sets.cfGauss = 0.0;
      chkCounter |= 1 << 1;
    }
    else if (sscanf(line, "CENTER_FIELD = auto - %lf", &dbl) == 1) {
      sets.cfAuto = 1;
      sets.cfShift = -dbl;
      sets.cfGauss = 0.0;
      chkCounter |= 1 << 1;
    }
    else if (sscanf(line, "CENTER_FIELD = %s", str) == 1 &&
	     !strcmp (str, "auto")) {
      sets.cfAuto = 1;
      sets.cfShift = 0.0;
      sets.cfGauss = 0.0;
      chkCounter |= 1 << 1;
    }
    else if (sscanf(line, "SWEEP_WIDTH = %lf", &sets.swGauss) == 1) {
      chkCounter |= 1 << 2;
    }
    else if (sscanf(line, "SCAN_TIME = %lf", &sets.swTime) == 1) {
      chkCounter |= 1 << 4;
    }
    else if (sscanf(line, "NUMBER_OF_POINTS = %zu", &sets.n) == 1) {
      chkCounter |= 1 << 6;
    }
    else if (sscanf(line, "NUMBER_OF_PROJECTIONS = %zu", &sets.k3) == 1) {
      chkCounter |= 1 << 9;
    }
    else if (sscanf(line, "NUMBER_OF_SCANS = %zu", &sets.nScans) == 1) {
      chkCounter |= 1 << 10;
    }
    else if (sscanf(line, "MAXIMUM_GRADIENT = %lf", &sets.maxGr) == 1) {
      chkCounter |= 1 << 13;
    }
    else if (sscanf(line, "GRADIENT_DISTRIBUTION = %s", str) == 1) {
      chkCounter |= 1 << 15;

      if (!strcmp(str, "gm3d") || !strcmp(str, "sphere")) {
	sets.grDistr = EPR::gm3d;
      }
      else if (!strcmp(str, "hcp") || !strcmp(str, "volume")) {
	sets.grDistr = EPR::hcp;
      }
      else if (!strcmp(str, "spi") || !strcmp(str, "cubic")) {
	sets.grDistr = EPR::spi;
      }
      else {
	sets.grDistr = EPR::custom;
	strcpy(sets.grFile, str);
      }
    }
    else if (sscanf(line, "GRADIENT_ORDER = %s", str) == 1) {
      chkCounter |= 1 << 16;

      if (!strcmp(str, "ascend")) {
	sets.grOrder = EPR::ascend;
      }
      else if (!strcmp(str, "descend")) {
	sets.grOrder = EPR::descend;
      }
      else if (!strcmp(str, "spiral")) {
	sets.grOrder = EPR::spiral;
      }
      else if (!strcmp(str, "random")) {
	sets.grOrder = EPR::random;
      }
      else if (!strcmp(str, "snake")) {
	sets.grOrder = EPR::snake;
      }
      else {
	printf("\t\e[1;35mWarning:\e[0m unknown gradient order"
	       " \"%s\"\n", str);
	chkCounter &= ~(1 << 16);
      }
    }
    else if (sscanf(line, "GRADIENT_DITHERING = %lf", &sets.dither) == 1) {
      chkCounter |= 1 << 18;
    }
  }

  return (chkCounter == SETS_ALL);
}

//==============================================================================

void auxiliarySettings(EPR::settings &sets, EPR::calibrations cals)
{
  double trigWidth;

  trigWidth = sets.n / (100.0 - cals.swOverhead) * cals.swOverhead;

  // odd+ number of points for symmetry
  sets.trigPulse = (size_t)floor(trigWidth / 2.0) * 2 + 1;

  sets.n_ = sets.n + sets.trigPulse;

  sets.swAdjusted = sets.swGauss * sets.n_ / sets.n;
  sets.syncDelay = cals.syncDelay;

  sets.nCycles = (sets.k3 * sets.nScans) / 2 + 1;	// integer division OK
  sets.nTrigs = sets.nCycles * 2;

  return;
}

//==============================================================================

void interpretCenterField(char *line, EPR::settings &sets)
{
  double value;
  char str[MAX_LENGTH];

  if (sscanf(line, "cf = %lf", &value) == 1) {
    sets.cfAuto = 0;
    sets.cfGauss = value;
    sets.cfShift = 0.0;
  }
  else if (sscanf(line, "cf + %lf", &value) == 1) {
    if (sets.cfAuto) {
      sets.cfGauss = 0.0;
      sets.cfShift += value;
    }
    else {
      sets.cfGauss += value;
      sets.cfShift = 0.0;
    }
  }
  else if (sscanf(line, "cf - %lf", &value) == 1) {
    if (sets.cfAuto) {
      sets.cfGauss = 0.0;
      sets.cfShift -= value;
    }
    else {
      sets.cfGauss -= value;
      sets.cfShift = 0.0;
    }
  }
  else if (sscanf(line, "cf = auto + %lf", &value) == 1) {
    sets.cfAuto = 1;
    sets.cfGauss = 0.0;
    sets.cfShift = value;
  }
  else if (sscanf(line, "cf = auto - %lf", &value) == 1) {
    sets.cfAuto = 1;
    sets.cfGauss = 0.0;
    sets.cfShift = -value;
  }
  else if (sscanf(line, "cf = %s", str) == 1 && !strcmp(str, "auto")) {
    sets.cfAuto = 1;
    sets.cfGauss = 0.0;
    sets.cfShift = 0.0;
  }

  return;
}

//==============================================================================

void interpretGradients(char *str, EPR::settings &sets)
{
  if (!strcmp(str, "gm3d") || !strcmp(str, "sphere")) {
    sets.grDistr = EPR::gm3d;
  }
  else if (!strcmp(str, "hcp") || !strcmp(str, "volume")) {
    sets.grDistr = EPR::hcp;
  }
  else if (!strcmp(str, "spi") || !strcmp(str, "cubic")) {
    sets.grDistr = EPR::spi;
  }
  else if (!strcmp(str, "ascend")) {
    sets.grOrder = EPR::ascend;
  }
  else if (!strcmp(str, "descend")) {
    sets.grOrder = EPR::descend;
  }
  else if (!strcmp(str, "spiral")) {
    sets.grOrder = EPR::spiral;
  }
  else if (!strcmp(str, "random")) {
    sets.grOrder = EPR::random;
  }
  else if (!strcmp(str, "snake")) {
    sets.grOrder = EPR::snake;
  }
  else {
    sets.grDistr = EPR::custom;
    strcpy(sets.grFile, str);
  }

  return;
}

//==============================================================================

void mergeProjections(std::vector<double> &prj0,
		      std::vector<float> &rawData,
		      EPR::settings &sets)
{
  size_t j, jj, no;

  std::vector<double> pr(sets.n, 0.0);
  prj0.assign(sets.k3 * sets.n, 0.0);

  for (j = 0; j < sets.k3; ++j) {
    for (jj = 0; jj < sets.nScans; ++jj) {

      no = j * sets.nScans + jj;
      std::copy_n(rawData.begin() + no * sets.n, sets.n, pr.begin());

      if (no % 2) {
	std::reverse(pr.begin(), pr.end());
      }

      std::transform(pr.begin(), pr.end(), prj0.begin() + j * sets.n,
		     prj0.begin() + j * sets.n, std::plus<double>());
    }
  }

  std::transform(prj0.begin(), prj0.end(), prj0.begin(),
		 std::bind(std::divides<double>(),
			   std::placeholders::_1, sets.nScans));

return;
}

//==============================================================================
//	Show usage information

void printHelp()
{
  printf("\t\e[1mh, help\e[0m\t\t\tdisplay this help\n");
  printf("\t\e[1ms, sets\e[0m\t\t\tshow current EPR settings\n");
  printf("\t\e[1mtune\e[0m\t\t\tgraphical tuning mode\n");
  printf("\t\e[1mrun, start\e[0m\t\tstart EPR image acquisition\n");
  printf("\t\e[1mshow\e[0m\t\t\tdisplay current EPR data\n");
  printf("\t\e[1mload filename\e[0m\t\tload EPR experiment\n");
  printf("\t\e[1msave [filename]\e[0m\t\tsave EPR experiment\n");
  printf("\t\e[1msave -r [filename]\e[0m\tsave raw EPR data\n");
  printf("\t\e[1mfrq\e[0m\t\t\treconfigure frequency counter\n"
	 "\t\t\t\tand report frequency value\n");
  printf("\t\e[1mcf [=/+/-] G\e[0m\t\tset or adjust center field\n");
  printf("\t\e[1mcf = auto [+/- G]\e[0m\tautomatic center field\n"
	 "\t\t\t\tbased on g-factor of 3CP radical\n");
  printf("\t\e[1msw = G\e[0m\t\t\tset sweep width\n");
  printf("\t\e[1mt1 = s\e[0m\t\t\tset single scan time\n");
  printf("\t\e[1mn = N\e[0m\t\t\tset number of points\n");
  printf("\t\e[1mk3 = N\e[0m\t\t\tset number of projections\n");
  printf("\t\e[1mns = N\e[0m\t\t\tset number of scans\n");
  printf("\t\e[1mgr = G/cm\e[0m\t\tset maximum gradient\n");
  printf("\t\e[1mgr = distr\e[0m\t\tset gradient distribution\n"
	 "\t\t\t\tsphere or gm3d, volume or hcp, cubic or spi\n");
  printf("\t\e[1mgr = order\e[0m\t\tset gradient order\n"
	 "\t\t\t\tascend, descend, spiral, random, snake)\n");
  printf("\t\e[1mgr = filename\e[0m\t\tload custom gradients\n");
  printf("\t\e[1mdither = X\e[0m\t\tadd artificial noise to the gradients\n");
  printf("\t\e[1mq, quit\e[0m\t\t\tquit EPR program\n");

  return;
}

//==============================================================================
//	Show current EPR settings

void printSets(EPR::settings &sets)
{
  char distr[MAX_LENGTH];
  char order[MAX_LENGTH];

  switch (sets.grDistr) {
  case EPR::gm3d:
    strcpy(distr, "gm3d sphere");
    break;

  case EPR::hcp:
    strcpy(distr, "hcp volume");
    break;

  case EPR::spi:
    strcpy(distr, "spi cubic");
    break;

  case EPR::custom:
    strcpy(distr, "custom");
    break;
  }

  switch (sets.grOrder) {
  case EPR::ascend:
    strcpy(order, "ascend");
    break;

  case EPR::descend:
    strcpy(order, "descend");
    break;

  case EPR::spiral:
    strcpy(order, "spiral");
    break;

  case EPR::random:
    strcpy(order, "random");
    break;

  case EPR::snake:
    strcpy(order, "snake");
    break;
  }

  if (sets.cfAuto == 1) {
    if (sets.cfShift >= 0.0) {
      printf("\tCenter field\t\tauto + %.3lf G\n", sets.cfShift);
    }
    else {
      printf("\tCenter field\t\tauto - %.3lf G\n", -sets.cfShift);
    }
  }
  else {
    printf("\tCenter field\t\t%.3lf G\n", sets.cfGauss);
  }

  printf("\tField sweep\t\t%.3lf G\n", sets.swGauss);
  printf("\tSweep time\t\t%.3le s\n", sets.swTime);
  printf("\tNumber of points\t%lu (+ %lu)\n", sets.n, sets.trigPulse);
  printf("\tNumber of projections\t%lu x %lu\n", sets.k3, sets.nScans);

  if (sets.grDistr == EPR::custom) {
    printf("\tGradients file\t\t%s (%s)\n", distr, sets.grFile);
  }
  else {
    printf("\tGradients\t\t%s (%s)\n", distr, order);
    printf("\tMaximum gradient\t%.3lf G/cm\n", sets.maxGr);
    printf("\tGradient dithering\t%.3lf\n", sets.dither);
  }

  return;
}

/*
void saveFieldSweep(std::vector<double> &swDataGauss, std::vector<float> &syncSignal)
{
	FILE *file;

	file = fopen("field.sw", "wb");
	if (file != NULL)
	{
		fwrite(swDataGauss.data(), sizeof(double), swDataGauss.size(), file);
		fwrite(syncSignal.data(), sizeof(float), syncSignal.size(), file);

		fclose(file);
	}

	return;
}
*/
