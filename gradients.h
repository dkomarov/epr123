// gradients.h
//
// Generates XYZ gradients with several types of distribution,
// optional random noise and different sorting methods
//

#include <vector>
#include "eprTypes.h"

void generateGradients(std::vector<double> &xyz, EPR::settings &sets);
