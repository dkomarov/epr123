// fileOps.h
//
// File operations, save and load EPR experiment
//

#include "eprTypes.h"

int saveExperiment(const char *fileName, EPR::experiment &exp);

int loadExperiment(const char *fileName, EPR::experiment &exp);

void saveRawData(const char *fileName, EPR::experiment &exp);

int genDescription(char *expDsc, EPR::experiment &exp);
