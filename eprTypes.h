// eprTypes.h
//

#ifndef eprTypes_h
#define eprTypes_h

#include <cstddef>
#include <ctime>

#include <vector>

#include <gsl/gsl_const_cgsm.h>

#define KB(n) ((size_t)(n) << 10)
#define MB(n) ((size_t)(n) << 20)

#define MAX_LENGTH 1024		// for strings
#define HRM_NUMBER 128		// sweep harmonics
#define SYNC_HIGH 3.3f		// high state, volt
#define G_FACTOR 2.0086		// 3CP
// #define G_FACTOR 2.0036	// DPPH
#define FRQ_BUFFER 10000	// enough for ~15 min

#define INSTALL_DIR "/usr/local/"
#define DAQ_DEVICES INSTALL_DIR "etc/daq.devices"
#define EPR_CONSTANTS INSTALL_DIR "etc/epr.constants"
#define DEFAULT_EXP INSTALL_DIR "etc/default.exp"
#define FRQ_COUNTER INSTALL_DIR "etc/frq.counter"
#define SW_CALIB_DIR INSTALL_DIR "etc/sw_calib.d/"
#define SINEWAVE_RSP SW_CALIB_DIR "sine.rsp"

namespace EPR {

  struct calibrations {
    double cfGauss1V;
    double cfGaussMax;

    double swGauss1V;
    double swGaussMax;

    double grGauss1cm1V[3];
    double grGauss1cmMax[3];

    double swOverhead;
    double syncDelay;
  };

  typedef enum {gm3d,
		hcp,
		spi,
		custom
  } grDistr_t;

  typedef enum {ascend,
		descend,
		spiral,
		random,
		snake
  } grOrder_t;

  struct settings {
    int cfAuto = 0;	// set field automatically using frequency readings
    double cfShift;	// ... and g-factor of 3CP radical +/- cfShift Gauss
    double cfGauss;	// ... or explicitly

    double swGauss;
    double swTime;

    double swAdjusted;
    double syncDelay;

    size_t n;		// number of points
    size_t n_;		// ... including trigger
    size_t trigPulse;

    size_t k3;		// number of gradients
    size_t nScans;	// number of scans

    size_t nCycles;	// number of sweep cycles
    size_t nTrigs;	// number of triggers

    double maxGr;
    double dither;

    EPR::grDistr_t grDistr;
    EPR::grOrder_t grOrder;

    char grFile[MAX_LENGTH];
  };

  struct experiment {
    EPR::settings sets;

    std::vector<double> swDataGauss;
    std::vector<double> xyzGauss1cm;
    std::vector<float> syncSignal;

    std::vector<float> rawData;		// forward and reverse scans as they are
    std::vector<double> prj0;		// flipped and averaged projections
    std::vector<double> frq;

    time_t start = 0;
    time_t end = 0;
  };

  typedef enum {offline,
		idle,
		badparam,
		rlperror,
		running,
		quitting,
	} status_t;
}

#endif	// eprTypes_h
