// Main program
//
// Reads the parameters from the configuration files,
// Sets up the hardware, communicates with the user,
// Handles errors and OS signals (TODO).
//

//#include <cmath>
#include <cstdio>
#include <cstdlib>
//#include <cstring>

#include <pthread.h>
#include <readline/history.h>
#include <readline/readline.h>

#include "eprTypes.h"
#include "supplement.h"
#include "fileOps.h"
#include "fieldSweep.h"
#include "gradients.h"
#include "eprInstrument.h"
#include "frqCounter.h"
#include "gtkFunctions.h"

EPR::status_t eprStatus;
pthread_mutex_t stLock;

frqCounter *eipPtr;
void *logFrequency(void *frqPtr);

//==============================================================================

int main(int argc, char *argv[])
{
  FILE *file;
  char *input;

  double dbl;
  size_t szt;
  char str[MAX_LENGTH];
  char fname[MAX_LENGTH];

  EPR::calibrations cals;
  EPR::settings sets;

  std::vector<double> swDataGauss;
  std::vector<double> xyzGauss1cm;
  std::vector<float> syncSignal;

  EPR::experiment exp;
  int unsaved = 0;

//==============================================================================
//	Bring up hardware, load calibration data and imaging settings

  if (!getEPRCalibrations(cals)) {
    printf("\t\e[1;31mError:\e[0m cannot read file \"%s\"\n", EPR_CONSTANTS);
    return -1;
  }

  file = fopen(DEFAULT_EXP, "r");
  if (file == NULL) {
    printf("\t\e[1;31mError:\e[0m cannot read file \"%s\"\n", DEFAULT_EXP);
    return -1;
  }

  if (!loadImagingSettings(file, sets)) {
    printf("\t\e[1;35mWarning:\e[0m file \"%s\" is corrupted\n", DEFAULT_EXP);

    fclose(file);
    return -1;
  }

  fclose(file);

  if (argc > 1 && loadExperiment(argv[1], exp)) {
    sets = exp.sets;
  }
  else {
    exp.sets = sets;
  }

  eprStatus = EPR::idle;

  eprInstrument epr(cals, eprStatus);	// may go offline
  epr.initialize(eprStatus);		// may go offline, TODO: DMA errors

  frqCounter eip;	// load ttyUSB location
  eip.open();		// open /dev/ttyUSB
  // configure Prologix USB adapter and EIP frequency counter
  eip.configure();	// sets devAvailable on success
  //  eip.flush();		// gives eip some time for configuration

  pthread_t frqThread;
  pthread_mutex_init(&stLock, NULL);
  eipPtr = &eip;

//==============================================================================
//	Main loop and user interface

  while (eprStatus != EPR::quitting) {

    if (eprStatus != EPR::offline) {
      eprStatus = EPR::idle;
    }

    // trigPulse, n_, nCycles, nTrigs, swAdjusted, syncDelay
    auxiliarySettings(sets, cals);

    // trying to reconfigure frequency counter if needed
    if (sets.cfAuto && !eip.isAvailable()) {
      eip.configure();
    }

    if (sets.cfAuto && eip.isAvailable()) {
      eip.flush();
      dbl = eip.getFrequency();	// zero if failed

      sets.cfGauss = dbl * GSL_CONST_CGSM_PLANCKS_CONSTANT_H
	/ (G_FACTOR * GSL_CONST_CGSM_BOHR_MAGNETON);
      sets.cfGauss += sets.cfShift;
    }

    generateFieldSweep(swDataGauss, syncSignal, sets);
    generateGradients(xyzGauss1cm, sets);

    epr.disarm(eprStatus);

    epr.updateConfiguration(sets, eprStatus);
    epr.setCenterField(sets.cfGauss, eprStatus);
    epr.prepAcquisition(sets, eprStatus);
    epr.primeFieldSweep(swDataGauss, syncSignal, eprStatus);
    epr.armGradients(xyzGauss1cm, sets, eprStatus);

    if (eprStatus == EPR::offline) {
      input = readline("\e[0;37mEPR:\e[0m ");
    } else if (eprStatus == EPR::badparam || eprStatus == EPR::rlperror) {
      input = readline("\e[1;31mEPR:\e[0m ");
    } else {
      input = readline("\e[1;32mEPR:\e[0m ");
    }

    if (input && *input) {
      add_history(input);
    }

    if (!strcmp(input, "h") || !strcmp(input, "help")) {
      printHelp();
    }
    else if (!strcmp(input, "s") || !strcmp(input, "sets")) {
      printSets(sets);
    }
    else if (!strcmp(input, "tune")) {
      epr.disarm(eprStatus);
      gTuningMode(epr, sets, eprStatus);
    }
    else if (!strcmp(input, "run") || !strcmp(input, "start")) {
      //========================================================================
      if (sets.cfAuto && eip.isAvailable()) {
	eip.flush();
	dbl = eip.getFrequency();	// zero if failed

	sets.cfGauss = dbl * GSL_CONST_CGSM_PLANCKS_CONSTANT_H
	  / (G_FACTOR * GSL_CONST_CGSM_BOHR_MAGNETON);
	sets.cfGauss += sets.cfShift;

	printf("\tFrequency = %.3lf MHz\tCenter field = %.3lf G\n",
	     dbl / 1.0e6, sets.cfGauss);

	epr.setCenterField(sets.cfGauss, eprStatus);
      }

      if (eprStatus != EPR::idle) {
	printf("\t\e[1;35mWarning:\e[0m EPR instrument is not ready\n");
	continue;
      }

      eprStatus = EPR::running;

      eip.flush();
      pthread_create(&frqThread, NULL, &logFrequency, &exp.frq);

      epr.runAcquisition(exp.rawData, sets, exp.start, exp.end);

      pthread_mutex_lock(&stLock);
      eprStatus = EPR::idle;
      pthread_mutex_unlock(&stLock);

      pthread_join(frqThread, NULL);

      exp.sets = sets;
      exp.swDataGauss = swDataGauss;
      exp.syncSignal = syncSignal;
      exp.xyzGauss1cm = xyzGauss1cm;

      mergeProjections(exp.prj0, exp.rawData, sets);
      unsaved = 1;
      //========================================================================
    }
    else if (!strcmp(input, "show")) {
      epr.disarm(eprStatus);
      gShowData(exp.prj0, exp.sets);
    }
    else if (sscanf(input, "load %s", fname) == 1) {
      if (loadExperiment(fname, exp)) {
	sets = exp.sets;
      }
    }
    else if (!strncmp(input, "save -r", 7)) {
      if (sscanf(input, "save -r %s", fname) != 1) {
	strcpy(fname, "noname");
      }
      saveRawData(fname, exp);
    }
    else if (!strncmp(input, "save -a", 7)) {
      if (sscanf(input, "save -a %s", fname) != 1) {
	strcpy(fname, "noname");
      }
      saveRawData(fname, exp);

      if (saveExperiment(fname, exp)) {
      unsaved = 0;
      }
    }
    else if (!strncmp(input, "save", 4)) {
      if (sscanf(input, "save %s", fname) != 1) {
	strcpy(fname, "noname");
      }
      if (saveExperiment(fname, exp)) {
      unsaved = 0;
      }
    }
    else if (!strncmp(input, "frq", 3)) {
      eip.configure();

      if (eip.isAvailable()) {
	eip.flush();
	dbl = eip.getFrequency();	// zero if failed

	printf("\tFrequency = %.3lf MHz\n", dbl / 1.0e6);
      }
    }
    else if (!strncmp(input, "cf", 2)) {
      interpretCenterField(input, sets);
    }
    else if (sscanf(input, "sw = %lf", &dbl) == 1) {
      sets.swGauss = dbl;
    }
    else if (sscanf(input, "t1 = %lf", &dbl) == 1) {
      sets.swTime = dbl;
    }
    else if (sscanf(input, "n = %lu", &szt) == 1) {
      sets.n = szt;
    }
    else if (sscanf(input, "k3 = %lu", &szt) == 1) {
      sets.k3 = szt;
    }
    else if (sscanf(input, "ns = %lu", &szt) == 1) {
      sets.nScans = szt;
    }
    else if (sscanf(input, "gr = %lf", &dbl) == 1) {
      sets.maxGr = dbl;
    }
    else if (sscanf(input, "gr = %s", str) == 1) {
      interpretGradients(str, sets);
    }
    else if (sscanf(input, "dither = %lf", &dbl) == 1) {
      sets.dither = dbl;
    }
    else if (!strcmp(input, "q") || !strcmp(input, "quit")) {
      eprStatus = EPR::quitting;

      if (unsaved) {
	printf("\tSaving unsaved data ...\n");
	strcpy(fname, "unsaved");
	saveExperiment(fname, exp);
      }
    }

    free(input);
  }

  pthread_mutex_destroy(&stLock);

  return 0;
}

//==============================================================================

void *logFrequency(void *frqPtr)
{
  EPR::status_t st = EPR::running;
  std::vector<double> &frq = *(static_cast<std::vector<double> *>(frqPtr));

  frq.clear();
  frq.reserve(FRQ_BUFFER);

  while (st == EPR::running && eipPtr->isAvailable()) {
    pthread_mutex_lock(&stLock);
    st = eprStatus;
    pthread_mutex_unlock(&stLock);

    frq.push_back(eipPtr->getFrequency());
  }

  frq.shrink_to_fit();

  return NULL;
}
