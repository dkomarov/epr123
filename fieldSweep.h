// fieldSweep.h
//
// Loads sweep calibration data from file and generates field sweep profile
//

#include <vector>

#include "eprTypes.h"

void generateFieldSweep(std::vector<double> &swDataGauss,
			std::vector<float> &syncSignal,
			EPR::settings &sets);
