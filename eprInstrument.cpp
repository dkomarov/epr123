// eprInstrument.cpp
//
// EPR instrument
//

#include <cmath>
#include <cstdio>
#include <cstring>

#include <algorithm>
#include <functional>

#include "eprInstrument.h"

//==============================================================================
//	Create an EPR object, load calibration constants, bring up DAQ devices

eprInstrument::eprInstrument(EPR::calibrations cals,
			     EPR::status_t &eprStatus)
  :cals(cals)
{
  FILE *file;
  char str[MAX_LENGTH];
  double value;
  int nBoards = 0;

  ai0Range = nNISTC3::kInput_10V;
  sampleRateMax = 0;

  file = fopen(DAQ_DEVICES, "r");
  if (file == NULL) {
    printf("\t\e[1;35mWarning:\e[0m cannot read file \"%s\"\n", DAQ_DEVICES);

    eprStatus = EPR::offline;
    return;
  }

  while (fgets(str, MAX_LENGTH, file)) {
    if (*str == '#' || *str == '\n') {
      continue;
    }
    else if (!strncmp(str, "PXI", 3) && nBoards < 2) {
      str[strcspn(str, "\n")] = 0;

      bus[nBoards] = acquireBoard(str);
      if (bus[nBoards] != NULL) {
	++nBoards;
      }
    }
    else if (sscanf(str, "INPUT_RANGE = %lf", &value) == 1) {
      if (value == 10.0) {
	ai0Range = nNISTC3::kInput_10V;
      }
      else if (value == 5.0) {
	ai0Range = nNISTC3::kInput_5V;
      }
      else if (value == 2.0) {
	ai0Range = nNISTC3::kInput_2V;
      }
      else if (value == 1.0) {
	ai0Range = nNISTC3::kInput_1V;
      }
      else if (value == 0.5) {
	ai0Range = nNISTC3::kInput_500mV;
      }
      else if (value == 0.2) {
	ai0Range = nNISTC3::kInput_200mV;
      }
      else if (value == 0.1) {
	ai0Range = nNISTC3::kInput_100mV;
      }
      else {
	printf("\t\e[1;35mWarning:\e[0m unknown DAQ input range,"
	       " using default 10 V\n");
      }
    }
    else if (sscanf(str, "MAXIMUM_SPEED = %lf", &value) == 1) {
      sampleRateMax = value;
    }
  }

  fclose(file);

  if (nBoards < 2) {
    printf("\t\e[1;35mWarning:\e[0m two DAQ devices are required,"
	   " %u found\n", nBoards);
    eprStatus = EPR::offline;
  }

  if (sampleRateMax == 0) {
    printf("\t\e[1;35mWarning:\e[0m file \"%s\" is corrupted\n", DAQ_DEVICES);
    eprStatus = EPR::offline;
  }

  return;
}

//==============================================================================
//	Initialize EPR instrument

void eprInstrument::initialize(EPR::status_t &eprStatus)
{
  if (eprStatus == EPR::offline)
    return;

  board0 = new tXSeries(bus[0]->createAddressSpace(kPCI_BAR0), &rlpStatus);

  board0_Info = nNISTC3::getDeviceInfo(*board0, rlpStatus);
  if (rlpStatus.isFatal()) {
    printf("\t\e[1;35mWarning:\e[0m unable to identify first DAQ device\n");

    eprStatus = EPR::offline;
    return;
  }

  board1 = new tXSeries(bus[1]->createAddressSpace(kPCI_BAR0), &rlpStatus);

  board1_Info = nNISTC3::getDeviceInfo(*board1, rlpStatus);
  if (rlpStatus.isFatal()) {
    printf("\t\e[1;35mWarning:\e[0m unable to identify second DAQ device\n");

    eprStatus = EPR::offline;
    return;
  }

  if (board0_Info->isSimultaneous)
    nNISTC3::initializeSimultaneousXSeries(*board0, rlpStatus);

  if (board1_Info->isSimultaneous)
    nNISTC3::initializeSimultaneousXSeries(*board1, rlpStatus);

  eeprom0 = new nNISTC3::eepromHelper(*board0, board0_Info->isSimultaneous,
				      board0_Info->numberOfADCs,
				      board0_Info->numberOfDACs, rlpStatus);

  eeprom1 = new nNISTC3::eepromHelper(*board1, board1_Info->isSimultaneous,
				      board1_Info->numberOfADCs,
				      board1_Info->numberOfDACs, rlpStatus);

  ai0Helper = new nNISTC3::aiHelper(*board0, board0_Info->isSimultaneous,
				    rlpStatus);

  ao0Helper = new nNISTC3::aoHelper(board0->AO, board0->AO.AO_Timer, rlpStatus);
  ao1Helper = new nNISTC3::aoHelper(board1->AO, board1->AO.AO_Timer, rlpStatus);

  ai0Stream = new nNISTC3::streamHelper(board0->AIStreamCircuit,
					board0->CHInCh, rlpStatus);

  ao0Stream = new nNISTC3::streamHelper(board0->AOStreamCircuit,
					board0->CHInCh, rlpStatus);

  ao1Stream = new nNISTC3::streamHelper(board1->AOStreamCircuit,
					board1->CHInCh, rlpStatus);

  ai0DMA = new nNISTC3::tCHInChDMAChannel(*board0, nNISTC3::kAI_DMAChannel,
					  rlpStatus);
  if (rlpStatus.isFatal()) {
    printf("\t\e[1;35mWarning:\e[0m input DMA channel initialization failed\n");

    eprStatus = EPR::offline;
    return;
  }

  ao0DMA = new nNISTC3::tCHInChDMAChannel(*board0, nNISTC3::kAO_DMAChannel,
					  rlpStatus);
  if (rlpStatus.isFatal()) {
    printf("\t\e[1;35mWarning:\e[0m field sweep DMA channel"
	   " initialization failed\n");

    eprStatus = EPR::offline;
    return;
  }

  ao1DMA = new nNISTC3::tCHInChDMAChannel(*board1, nNISTC3::kAO_DMAChannel,
					  rlpStatus);
  if (rlpStatus.isFatal()) {
    printf("\t\e[1;35mWarning:\e[0m gradients DMA channel"
	   " initialization failed\n");

    eprStatus = EPR::offline;
    return;
  }

  ai0Gain = board0_Info->getAI_Gain(ai0Range, rlpStatus);
  if (rlpStatus.isFatal()) {
    printf("\t\e[1;35mWarning:\e[0m unsupported DAQ input range\n");

    eprStatus = EPR::offline;
    return;
  }

  ao0Gain = board0_Info->getAO_Gain(ao0Range, rlpStatus);
  ao1Gain = board1_Info->getAO_Gain(ao1Range, rlpStatus);

  ai0Config[0].isLastChannel = kTrue;
  ai0Config[0].enableDither = nAI::kEnabled;
  ai0Config[0].gain = ai0Gain;
  ai0Config[0].channelType = nAI::kDifferential;
  ai0Config[0].bank = nAI::kBank0;
  ai0Config[0].channel = 0;
  ai0Config[0].range = ai0Range;

  for (unsigned int ich = 0; ich < 2; ++ich) {
    ao0Config[ich].channel = ich;
    ao0Config[ich].gain = ao0Gain;
    ao0Config[ich].updateMode = nAO::kTimed;
    ao0Config[ich].range = ao0Range;
  }

  ao1Config[0].channel = 0;
  ao1Config[0].gain = ao1Gain;
  ao1Config[0].updateMode = nAO::kImmediate;
  ao1Config[0].range = ao1Range;

  for (unsigned int ich = 1; ich < 4; ++ich) {
    ao1Config[ich].channel = ich;
    ao1Config[ich].gain = ao1Gain;
    ao1Config[ich].updateMode = nAO::kTimed;
    ao1Config[ich].range = ao1Range;
  }

  nNISTC3::nAODataHelper::scaleData(std::vector<float>(2, 0), 2,
				    zerosDAC0, 2, ao0Config, *eeprom0);

  nNISTC3::nAODataHelper::scaleData(std::vector<float>(4, 0), 4,
				    zerosDAC1, 4, ao1Config, *eeprom1);

  // set all PFI to be input
  board0->Triggers.PFI_Direction_Register.writeRegister(0x0, &rlpStatus);
  board1->Triggers.PFI_Direction_Register.writeRegister(0x0, &rlpStatus);

  ai0Helper->reset(rlpStatus);
  ao0Helper->reset(rlpStatus);
  ao1Helper->reset(rlpStatus);

  board0->AO.AO_Direct_Data[0].writeRegister(zerosDAC0[0], &rlpStatus);
  board0->AO.AO_Direct_Data[1].writeRegister(zerosDAC0[1], &rlpStatus);

  board1->AO.AO_Direct_Data[0].writeRegister(zerosDAC1[0], &rlpStatus);
  board1->AO.AO_Direct_Data[1].writeRegister(zerosDAC1[1], &rlpStatus);
  board1->AO.AO_Direct_Data[2].writeRegister(zerosDAC1[2], &rlpStatus);
  board1->AO.AO_Direct_Data[3].writeRegister(zerosDAC1[3], &rlpStatus);

  if (rlpStatus.isFatal()) {
    printf("\t\e[1;35mWarning:\e[0m an error occured "
	   "during DAQ devices initialization\n");
    eprStatus = EPR::offline;
  }

  return;
}

//==============================================================================
//	Configure DAQ Boards

void eprInstrument::updateConfiguration(EPR::settings &sets,
					EPR::status_t &eprStatus)
{
  if (eprStatus == EPR::offline) {
    return;
  }

  rlpStatus.statusCode = kStatusSuccess;

  const unsigned int updateInterval = round(sets.swTime / sets.n_ * TB3);
  const unsigned int samplePeriod = updateInterval;

  // convert period is not important for single channel AI
  const unsigned int convertPeriod = 10; // 100 ns

//==============================================================================
//	Configure ADC Board 0, epr signal

  board0->AI.AI_Timer.Reset_Register.\
    writeConfiguration_Start(kTrue, &rlpStatus);

  ai0Helper->programExternalGate( // no external gating
				 nAI::kGate_Disabled,
				 nAI::kActive_High_Or_Rising_Edge,
				 rlpStatus);

  ai0Helper->programStart1( // start on trigger pullse from PFI0
			   nAI::kStart1_PFI0,
			   nAI::kActive_Low_Or_Falling_Edge,
			   kTrue, rlpStatus);

  ai0Helper->programStart( // sample clock source
			  nAI::kStartCnv_InternalTiming,
			  nAI::kActive_High_Or_Rising_Edge,
			  kTrue, rlpStatus);

  if (board0_Info->isSimultaneous) {
    // internal timing with the same polarity as sample clock for SMIO devices
    ai0Helper->programConvert(nAI::kStartCnv_InternalTiming,
			      nAI::kActive_High_Or_Rising_Edge,
			      rlpStatus);
  } else {
    // internal or external active low for MIO devices as required by specs
    ai0Helper->programConvert(nAI::kStartCnv_InternalTiming,
			      nAI::kActive_Low_Or_Falling_Edge,
			      rlpStatus);
  }

  inTimingConfig.setAcqLevelTimingMode( // post trigger acquisition
				       nNISTC3::kInTimerPostTrigger,
				       rlpStatus);

  inTimingConfig.setUseSICounter( // use SI counter for sample clock
				 kTrue, rlpStatus);

  inTimingConfig.setSamplePeriod( // sample period in TB3 ticks
				 samplePeriod, rlpStatus);

  inTimingConfig.setSampleDelay( // TB3 ticks delay before sampling
				2, rlpStatus);

  inTimingConfig.setNumberOfSamples( // number of samples
				    sets.n, rlpStatus);

  inTimingConfig.setRetriggerRecord( // retriggerable acquisition
				    kTrue, rlpStatus);

  if (!board0_Info->isSimultaneous) {
    inTimingConfig.setUseSI2Counter( // use SI2 counter for conver clock
				    kTrue, rlpStatus);

    inTimingConfig.setConvertPeriod( // convert period in TB3 ticks
				    convertPeriod, rlpStatus);

    inTimingConfig.setConvertDelay( // TB3 ticks delay before convert
				   2, rlpStatus);
  }

  ai0Helper->getInTimerHelper(rlpStatus).programTiming(inTimingConfig,
						       rlpStatus);

  ai0Helper->programFIFOWidth(nAI::kTwoByteFifo, rlpStatus);

  ai0Helper->getInTimerHelper(rlpStatus).clearConfigurationMemory(rlpStatus);

  ai0Helper->programChannel(ai0Config[0], rlpStatus);

  ai0Helper->getInTimerHelper(rlpStatus).primeConfigFifo(rlpStatus);

  board0->AI.AI_Timer.Reset_Register.writeConfiguration_End(kTrue, &rlpStatus);

//==============================================================================
//	Configure DAC Board 0, field sweep & trigger

  // NI's bug workaround, fixing the wrong number of output channels
  board0->AO.AO_Timer.Output_Control_Register.markDirty(&rlpStatus);

  board0->AO.AO_Timer.Reset_Register.\
    writeConfiguration_Start(kTrue, &rlpStatus);

  ao0Helper->programExternalGate( // external gating disabled
				 nAO::kGate_Disabled,
				 nAO::kRising_Edge,
				 kFalse, rlpStatus);

  ao0Helper->programStart1( // software asserted START1
			   nAO::kStart1_Pulse,
			   nAO::kRising_Edge,
			   kTrue, rlpStatus);

  ao0Helper->getOutTimerHelper(rlpStatus).\
    programStart1( // triggers synchronized with UI source
		  nOutTimer::kSyncDefault,
		  nOutTimer::kExportSynchronizedTriggers,
		  rlpStatus);

  ao0Helper->getOutTimerHelper(rlpStatus).\
    programBufferCount(sets.nCycles, rlpStatus);	// number of cycles

  ao0Helper->getOutTimerHelper(rlpStatus).\
    programUpdateCount(sets.n_ * 2, 0, rlpStatus);	// number of points

  ao0Helper->getOutTimerHelper(rlpStatus).loadUC(rlpStatus);

  ao0Helper->getOutTimerHelper(rlpStatus).\
    programBCGate(nOutTimer::kDisabled, rlpStatus);	// no external gating

  ao0Helper->programUpdate( // update on UI terminal count
			   nAO::kUpdate_UI_TC,
			   nAO::kRising_Edge, rlpStatus);

  ao0Helper->getOutTimerHelper(rlpStatus).\
    programUICounter( // internal TB3 clock source for UI
		     nOutTimer::kUI_Src_TB3,
		     nOutTimer::kRising_Edge,
		     // finite operation mode
		     nOutTimer::kFiniteOp,
		     rlpStatus);

  ao0Helper->getOutTimerHelper(rlpStatus).\
    loadUI( // TB3 ticks delay and update interval
	   2, updateInterval, rlpStatus);

  ao0Helper->getOutTimerHelper(rlpStatus).\
    programStopCondition(kTrue,			// trigger once
			 nOutTimer::kFiniteOp,		// finite mode
			 nOutTimer::kStop_on_Error,	// BC_TC error
			 nOutTimer::kStop_on_Error,	// BC_TC trigger error
			 nOutTimer::kStop_on_Error,	// ovverrun error
			 rlpStatus);

  ao0Helper->getOutTimerHelper(rlpStatus).\
    programFIFO(kTrue,		// enable FIFO
		// DMA data request condition
		nOutTimer::kFifoMode_Less_Than_Full,
		// onboard data regeneration
		nOutTimer::kEnabled,
		rlpStatus);

  ao0Helper->getOutTimerHelper(rlpStatus).clearFIFO(rlpStatus);

  ao0Helper->getOutTimerHelper(rlpStatus).programNumberOfChannels(2, rlpStatus);

  ao0Helper->programConfigBank(ao0Config[0], rlpStatus);
  ao0Helper->programConfigBank(ao0Config[1], rlpStatus);

  ao0Helper->programChannels( // order in data buffer
			     ao0Config, rlpStatus);

  board0->AO.AO_Timer.Reset_Register.writeConfiguration_End(kTrue, &rlpStatus);

  ao0Helper->setZeroVolts(zerosDAC0, kTrue, rlpStatus);

//==============================================================================
//	Configure DAC Board 1, center field & gradients

  // NI's bug workaround, fixing the wrong number of output channels
  board1->AO.AO_Timer.Output_Control_Register.markDirty(&rlpStatus);

  board1->AO.AO_Timer.Reset_Register.\
    writeConfiguration_Start(kTrue, &rlpStatus);

  ao1Helper->programExternalGate( // no external gating
				 nAO::kGate_Disabled,
				 nAO::kRising_Edge,
				 kFalse, rlpStatus);

  ao1Helper->programStart1( // software asserted START1
			   nAO::kStart1_Pulse,
			   nAO::kRising_Edge,
			   kTrue, rlpStatus);

  ao1Helper->getOutTimerHelper(rlpStatus).\
    programStart1( // triggers synchronized with UI source
		  nOutTimer::kSyncDefault,
		  nOutTimer::kExportSynchronizedTriggers,
		  rlpStatus);

  ao1Helper->getOutTimerHelper(rlpStatus).\
    programBufferCount(1, rlpStatus);			// loop count

  ao1Helper->getOutTimerHelper(rlpStatus).\
    programUpdateCount(sets.nTrigs, 0, rlpStatus);	// number of gradients

  ao1Helper->getOutTimerHelper(rlpStatus).loadUC(rlpStatus);

  ao1Helper->getOutTimerHelper(rlpStatus).\
    programBCGate(nOutTimer::kDisabled, rlpStatus);	// no external gating

  ao1Helper->programUpdate( // update on trigger pulse from PFI0
			   nAO::kUpdate_PFI0,
			   nAO::kRising_Edge, rlpStatus);

  ao1Helper->getOutTimerHelper(rlpStatus).\
    programStopCondition(kTrue,			// trigger once
			nOutTimer::kFiniteOp,		// finite mode
			nOutTimer::kStop_on_Error,	// BC_TC error
			nOutTimer::kStop_on_Error,	// BC_TC trigger error
			nOutTimer::kStop_on_Error,	// ovverrun error
			rlpStatus);

  ao1Helper->getOutTimerHelper(rlpStatus).\
    programFIFO(kTrue,		// enable FIFO
		// DMA data request condition
		nOutTimer::kFifoMode_Less_Than_Full,
		// onboard data regeneration
		nOutTimer::kDisabled, rlpStatus);

  ao1Helper->getOutTimerHelper(rlpStatus).clearFIFO(rlpStatus);

  ao1Helper->getOutTimerHelper(rlpStatus).programNumberOfChannels(3, rlpStatus);

  for (unsigned int ich = 0; ich < 4; ++ich) {
    // channel 0 for center field, channels 1 - 3 for the gradients
    ao1Helper->programConfigBank(ao1Config[ich], rlpStatus);
  }

  ao1Helper->programChannels( // order in data buffer
		     std::vector<nNISTC3::aoHelper::tChannelConfiguration>
		     (ao1Config.begin() + 1, ao1Config.end()), rlpStatus);

  board1->AO.AO_Timer.Reset_Register.writeConfiguration_End(kTrue, &rlpStatus);

  ao1Helper->setZeroVolts(zerosDAC1, kTrue, rlpStatus);

  if (rlpStatus.isFatal()) {
    printf("\t\e[1;35mWarning:\e[0m an error occured "
	   "during DAQ boards configuration\n");
    eprStatus = EPR::rlperror;
  }

  return;
}

//==============================================================================
//	Set center field

void eprInstrument::setCenterField(double cfGauss, EPR::status_t &eprStatus)
{
  if (eprStatus == EPR::offline || eprStatus == EPR::rlperror) {
    return;
  }
  else if (fabs(cfGauss) > cals.cfGaussMax) {
    printf("\t\e[1;35mWarning:\e[0m main field exceeds maximum for the coil\n");

    eprStatus = EPR::badparam;
    return;
  }

  float cfVolt = cfGauss / cals.cfGauss1V;
  std::vector<short> cf4DAC(1, 0);

  std::vector<nNISTC3::aoHelper::tChannelConfiguration>
    chConfig(ao1Config.begin(), ao1Config.begin() + 1);

  nNISTC3::nAODataHelper::scaleData(std::vector<float>(1, cfVolt),
				    1, cf4DAC, 1, chConfig, *eeprom1);

  board1->AO.AO_Direct_Data[0].writeRegister(cf4DAC[0], &rlpStatus);

  board1->AO.AO_Timer.Status_1_Register.refresh(&rlpStatus);

  if (board1->AO.AO_Timer.Status_1_Register.getOverrun_St(&rlpStatus)) {
    printf("\t\e[1;35mWarning:\e[0m center field DAC overrun\n");
    eprStatus = EPR::rlperror;
  }

  return;
}

//==============================================================================
//	Prepare AI DMA channel and arm AI timing circuitry

void eprInstrument::prepAcquisition(EPR::settings &sets,
				    EPR::status_t &eprStatus)
{
  if (eprStatus == EPR::offline || eprStatus == EPR::rlperror) {
    return;
  }
  else if (sets.n_ / sets.swTime > sampleRateMax) {
    printf("\t\e[1;35mWarning:\e[0m sampling rate exceeds"
	   " DAQ board specifications\n");

    eprStatus = EPR::badparam;
    return;
  }

  size_t singleScanBytes = sets.n * sizeof(short);
  size_t ai0DMABufferSize = singleScanBytes * DMA_FACTOR;

  ai0DMA->reset(rlpStatus);

  ai0DMA->configure(bus[0], nNISTC3::kReuseLinkRing,	// scatter-gather DMA
		    nNISTC3::kIn, ai0DMABufferSize, rlpStatus);

  if (rlpStatus.isFatal()) {
    printf("\t\e[1;35mWarning:\e[0m AI DMA channel configuration failed\n");

    eprStatus = EPR::rlperror;
    return;
  }

  ai0DMA->start(rlpStatus);

  if (rlpStatus.isFatal()) {
    printf("\t\e[1;35mWarning:\e[0m AI DMA channel failed to start\n");

    eprStatus = EPR::rlperror;
    return;
  }

  ai0Stream->configureForInput(kTrue,		// DMA throttling by CISTCR
			nNISTC3::kAI_DMAChannel, rlpStatus);

  ai0Stream->modifyTransferSize(ai0DMABufferSize, rlpStatus);	// set CISTCR

  ai0Stream->enable(rlpStatus);

  ai0Helper->getInTimerHelper(rlpStatus).armTiming(inTimingConfig, rlpStatus);

  // ... waiting for PFI0
  return;
}

//==============================================================================
//	Check field sweep

int eprInstrument::chkFieldSweep(std::vector<double> &swDataGauss)
{
  double min, max;

  min = *std::min_element(swDataGauss.begin(), swDataGauss.end());
  max = *std::max_element(swDataGauss.begin(), swDataGauss.end());

  if (min < -cals.swGaussMax / 2.0 || max > cals.swGaussMax / 2.0) {
    return 0;
  }

  return 1;
}

//==============================================================================
//	Load field sweep data into AO FIFO buffer

void eprInstrument::primeFieldSweep(std::vector<double> &swDataGauss,
				    std::vector<float> &syncSignal,
				    EPR::status_t &eprStatus)
{
  if (eprStatus == EPR::offline || eprStatus == EPR::rlperror) {
    return;
  }
  else if (!chkFieldSweep(swDataGauss)) {
    printf("\t\e[1;35mWarning:\e[0m field sweep "
	   "exceeds maximum allowed for the coil\n");

    eprStatus = EPR::badparam;
    return;
  }

  size_t n_2 = swDataGauss.size();
  size_t aoDataBytes = n_2 * 2 * sizeof(short);

  unsigned int availableSpace = 0;
  unsigned int dataRegenerated = kFalse;

  double rlpTime;
  clock_t rlpStart;

  if (aoDataBytes > ao0Stream->getFifoSize()) {
    printf("\t\e[1;35mWarning:\e[0m field sweep data "
	   "exceeds AO FIFO buffer size\n");

    eprStatus = EPR::badparam;
    return;
  }

  std::vector<float> swDataVolt(n_2, 0.0f);
  std::vector<float> jointDataVolt(n_2 * 2, 0.0f);
  std::vector<short> jointData4DAC(n_2 * 2, 0);

  std::transform(swDataGauss.begin(), swDataGauss.end(), swDataVolt.begin(),
		 std::bind(std::divides<double>(),
			   std::placeholders::_1, cals.swGauss1V));

  nNISTC3::nAODataHelper::interleaveData(n_2, 2, jointDataVolt,
					 swDataVolt, syncSignal);

  nNISTC3::nAODataHelper::scaleData(jointDataVolt, n_2 * 2,
				    jointData4DAC, n_2 * 2,
				    ao0Config, *eeprom0);

  ao0DMA->reset(rlpStatus);

  ao0DMA->configure(bus[0], nNISTC3::kNormal,	// DMA topology and buffer size
		    nNISTC3::kOut, aoDataBytes, rlpStatus);

  if (rlpStatus.isFatal()) {
    printf("\t\e[1;35mWarning:\e[0m field sweep DMA channel "
	   "configuration failed\n");

    eprStatus = EPR::rlperror;
    return;
  }

  ao0DMA->write(aoDataBytes,
		reinterpret_cast<unsigned char *>(&jointData4DAC[0]),
		&availableSpace, kFalse,	// do not allow regeneration
		&dataRegenerated, rlpStatus);

  if (rlpStatus.isFatal()) {
    printf("\t\e[1;35mWarning:\e[0m failed to write "
	   "field sweep data into DMA buffer\n");

    eprStatus = EPR::rlperror;
    return;
  }

  ao0DMA->start(rlpStatus);

  if (rlpStatus.isFatal()) {
    printf("\t\e[1;35mWarning:\e[0m field sweep DMA channel failed to start\n");

    eprStatus = EPR::rlperror;
    return;
  }

  ao0Stream->configureForOutput(kTrue,		// use CISTCR for DMA data
				nNISTC3::kAO_DMAChannel, rlpStatus);

  ao0Stream->modifyTransferSize(aoDataBytes, rlpStatus);	// set CISTCR

  ao0Stream->enable(rlpStatus);

  rlpTime = 0;
  rlpStart = clock();

  while (board0->AO.AO_FIFO_Status_Register.\
	 readRegister(&rlpStatus) < n_2 * 2) {

    if (rlpTime > RLP_TIMEOUT) {
      printf("\t\e[1;35mWarning:\e[0m field sweep FIFO buffer timeout\n");
      eprStatus = EPR::rlperror;
      return;
    }

    rlpTime = static_cast<double>(clock() - rlpStart) / CLOCKS_PER_SEC;
  }

  ao0Stream->disable(rlpStatus);
  ao0DMA->stop(rlpStatus);	// use data from FIFO buffer only

  ao0Helper->getOutTimerHelper(rlpStatus).notAnUpdate(rlpStatus);

  ao0Helper->getOutTimerHelper(rlpStatus).setArmUI(kTrue, rlpStatus);
  ao0Helper->getOutTimerHelper(rlpStatus).setArmUC(kTrue, rlpStatus);
  ao0Helper->getOutTimerHelper(rlpStatus).setArmBC(kTrue, rlpStatus);
  ao0Helper->getOutTimerHelper(rlpStatus).armTiming(rlpStatus);

  // ... waiting for START1 pulse
  return;
}

//==============================================================================
//	Check gradients

int eprInstrument::chkGradients(std::vector<double> &xyzGauss1cm)
{
  size_t k3;
  double min, max;
  int fail = 0;

  k3 = xyzGauss1cm.size() / 3;

  if (xyzGauss1cm.size() % 3) {
    printf("\t\e[1;35mFix me:\e[0m wrong gradient data size\n");
    return 0;
  }

  min = *std::min_element(&xyzGauss1cm[0], &xyzGauss1cm[k3 - 1]);
  max = *std::max_element(&xyzGauss1cm[0], &xyzGauss1cm[k3 - 1]);

  if (min < -cals.grGauss1cmMax[0] || max > cals.grGauss1cmMax[0]) {
    printf("\t\e[1;35mWarning:\e[0m X gradient exceeds the maximum allowed\n");
    fail = 1;
  }

  min = *std::min_element(&xyzGauss1cm[k3], &xyzGauss1cm[k3 * 2 - 1]);
  max = *std::max_element(&xyzGauss1cm[k3], &xyzGauss1cm[k3 * 2 - 1]);

  if (min < -cals.grGauss1cmMax[1] || max > cals.grGauss1cmMax[1]) {
    printf("\t\e[1;35mWarning:\e[0m Y gradient exceeds the maximum allowed\n");
    fail = 1;
  }

  min = *std::min_element(&xyzGauss1cm[k3 * 2], &xyzGauss1cm[k3 * 3 - 1]);
  max = *std::max_element(&xyzGauss1cm[k3 * 2], &xyzGauss1cm[k3 * 3 - 1]);

  if (min < -cals.grGauss1cmMax[2] || max > cals.grGauss1cmMax[2]) {
    printf("\t\e[1;35mWarning:\e[0m Z gradient exceeds the maximum allowed\n");
    fail = 1;
  }

  return !fail;
}

//==============================================================================
//	Scale, reorder and clone gradients

void eprInstrument::scaleGradients(std::vector<float> &xyzVolt,
				   std::vector<double> &xyzGauss1cm,
				   EPR::settings &sets)
{
  size_t i, j, nj;
  size_t k3 = sets.k3;
  size_t nScans = sets.nScans;

  for (j = 0; j < k3; ++j) {
    nj = nScans * j * 3;

    for (i = 0; i < nScans; ++i) {
      xyzVolt[nj + i * 3] = xyzGauss1cm[j] / cals.grGauss1cm1V[0];
      xyzVolt[nj + i * 3 + 1] = xyzGauss1cm[k3 + j] / cals.grGauss1cm1V[1];
      xyzVolt[nj + i * 3 + 2] = xyzGauss1cm[k3 * 2 + j] / cals.grGauss1cm1V[2];
    }
  }

  return;
}

//==============================================================================
//	Prepare DMA channel for the gradients and arm AO timing circuitry
//	TODO: optimized scatter-gather DMA

void eprInstrument::armGradients(std::vector<double> &xyzGauss1cm,
				 EPR::settings &sets,
				 EPR::status_t &eprStatus)
{
  if (eprStatus == EPR::offline || eprStatus == EPR::rlperror) {
    return;
  }
  else if (!chkGradients(xyzGauss1cm)) {
    eprStatus = EPR::badparam;
    return;
  }
  else if (sets.k3 * sets.nScans > MB(4)) {
    printf("\t\e[1;35mWarning:\e[0m gradient data exceed DMA buffer size\n");

    eprStatus = EPR::badparam;
    return;
  }

  size_t numberOfSamples = sets.nTrigs * 3;
  size_t aoDataBytes = numberOfSamples * sizeof(short);
  unsigned int availableSpace = 0;
  unsigned int dataRegenerated = kFalse;

  std::vector<float> xyzVolt(numberOfSamples, 0.0f);
  std::vector<short> xyz4DAC(numberOfSamples, 0);

  scaleGradients(xyzVolt, xyzGauss1cm, sets);

  std::vector<nNISTC3::aoHelper::tChannelConfiguration>
    chConfig(ao1Config.begin() + 1, ao1Config.end());

  nNISTC3::nAODataHelper::scaleData(xyzVolt, numberOfSamples,
				    xyz4DAC, numberOfSamples,
				    chConfig, *eeprom1);

  ao1DMA->reset(rlpStatus);

  ao1DMA->configure(bus[1], nNISTC3::kNormal,	// DMA topology and buffer size
		    nNISTC3::kOut, aoDataBytes, rlpStatus);

  if (rlpStatus.isFatal()) {
    printf("\t\e[1;35mWarning:\e[0m gradients DMA channel "
	   "configuration failed\n");

    eprStatus = EPR::rlperror;
    return;
  }

  ao1DMA->write(aoDataBytes, reinterpret_cast<unsigned char *>(&xyz4DAC[0]),
		&availableSpace, kFalse,	// do not allow regeneration
		&dataRegenerated, rlpStatus);

  if (rlpStatus.isFatal()) {
    printf("\t\e[1;35mWarning:\e[0m failed to write gradients data "
	   "into DMA buffer\n");

    eprStatus = EPR::rlperror;
    return;
  }

  ao1DMA->start(rlpStatus);

  if (rlpStatus.isFatal()) {
    printf("\t\e[1;35mWarning:\e[0m gradients DMA channel failed to start\n");

    eprStatus = EPR::rlperror;
    return;
  }

  ao1Stream->configureForOutput(kTrue,		// use CISTCR for DMA data
				nNISTC3::kAO_DMAChannel, rlpStatus);

  ao1Stream->modifyTransferSize(aoDataBytes, rlpStatus);	// set CISTCR

  ao1Stream->enable(rlpStatus);

  ao1Helper->getOutTimerHelper(rlpStatus).notAnUpdate(rlpStatus);

  ao1Helper->getOutTimerHelper(rlpStatus).setArmUC(kTrue, rlpStatus);
  ao1Helper->getOutTimerHelper(rlpStatus).setArmBC(kTrue, rlpStatus);
  ao1Helper->getOutTimerHelper(rlpStatus).armTiming(rlpStatus);

  // ... waiting for START1 pulse
  return;
}

//==============================================================================
//	Run field sweep, gradients and data acquisition

void eprInstrument::runAcquisition(std::vector<float> &eprData,
				   EPR::settings &sets,
				   time_t &start, time_t &end)
{
  // no status check here, because of the pthread mutex in the main program
  // all errors are reported as "Runtime errors", EPR status is not tracked

  size_t scanCounter = 0;
  size_t singleScanBytes = sets.n * sizeof(short);
  unsigned int bytesAvailable = 0;
  unsigned int dataOverwritten = kFalse;

  std::vector<short> rawADC_Data(sets.n, 0);
  std::vector<float> adcDataVolt(sets.n, 0);
  eprData.resize(sets.n * sets.nTrigs);

  double chkInterval = sets.nTrigs / 101.0;	// prime number > 100
  double chkPoint = chkInterval;

  unsigned int eprError = 0;

  double rlpTime;
  clock_t rlpStart;
  time_t elapsedTime;

//==============================================================================
//	Start field sweep and data aquisition

  time(&start);

  printf("\tEPR is running ");
  fflush(stdout);

  // start gradients
  board1->AO.AO_Timer.Command_1_Register.writeSTART1_Pulse(kTrue, &rlpStatus);

  // start field sweep
  board0->AO.AO_Timer.Command_1_Register.writeSTART1_Pulse(kTrue, &rlpStatus);

  cnt = 0;	// DBG purposes
  while (!board0->AO.AO_Timer.Status_1_Register.readBC_TC_St(&rlpStatus)) {
    ai0DMA->read(0, NULL, &bytesAvailable, kFalse, &dataOverwritten, rlpStatus);

    if (rlpStatus.isFatal()) {
      putchar('x');
      eprError |= 1 << 5;
      break;
    }

    if (bytesAvailable >= singleScanBytes) {

      ai0DMA->read(singleScanBytes,
		   reinterpret_cast<unsigned char *>(&rawADC_Data[0]),
		   &bytesAvailable, kFalse, &dataOverwritten, rlpStatus);

      if (rlpStatus.isFatal()) {
	putchar('x');
	eprError |= 1 << 5;
	break;
      }

      if (scanCounter == sets.nTrigs) {
	putchar('x');
	eprError |= 1 << 6;
	break;
      }

      ai0Stream->modifyTransferSize(singleScanBytes, rlpStatus);

      nNISTC3::nAIDataHelper::scaleData(rawADC_Data, sets.n,
					adcDataVolt, sets.n,
					ai0Config, *eeprom0, *board0_Info);

      std::copy_n(adcDataVolt.begin(), sets.n,
		  eprData.begin() + sets.n * scanCounter);

      ++scanCounter;
    }

    // Check AI errors
    board0->AI.AI_Timer.Status_1_Register.refresh(&rlpStatus);

    if (board0->AI.AI_Timer.Status_1_Register.getScanOverrun_St(&rlpStatus)) {
      eprError |= 1 << 1;
      putchar('x');
      break;
    }

    if (board0->AI.AI_Timer.Status_1_Register.getOverrun_St(&rlpStatus)) {
      eprError |= 1 << 2;
      putchar('x');
      break;
    }

    if (board0_Info->isSimultaneous && board0->SimultaneousControl.\
	InterruptStatus.readAiFifoOverflowInterruptCondition(&rlpStatus)) {
      eprError |= 1 << 3;
      putchar('x');
      break;
    }

    if (!board0_Info->isSimultaneous && board0->AI.AI_Timer.\
	Status_1_Register.getOverflow_St(&rlpStatus)) {
      eprError |= 1 << 3;
      putchar('x');
      break;
    }

    // Check board0 AO errors
    board0->AO.AO_Timer.Status_1_Register.refresh(&rlpStatus);

    if (board0->AO.AO_Timer.Status_1_Register.getUnderflow_St(&rlpStatus)) {
      eprError |= 1 << 9;
      putchar('x');
      break;
    }

    if (board0->AO.AO_Timer.Status_1_Register.getOverrun_St(&rlpStatus)) {
      eprError |= 1 << 10;
      putchar('x');
      break;
    }

    // Check board1 AO errors
    board1->AO.AO_Timer.Status_1_Register.refresh(&rlpStatus);

    if (board1->AO.AO_Timer.Status_1_Register.getUnderflow_St(&rlpStatus)) {
      eprError |= 1 << 17;
      putchar('x');
      break;
    }

    if (board1->AO.AO_Timer.Status_1_Register.getOverrun_St(&rlpStatus)) {
      eprError |= 1 << 18;
      putchar('x');
      break;
    }

    // Show progress
    if (static_cast<double>(scanCounter) > chkPoint) {
      putchar('.');
      fflush(stdout);

      if (fabs(chkPoint - chkInterval * 50.0) < 1e-6) {
	printf("\n\t\t       ");
	fflush(stdout);
      }

      chkPoint += chkInterval;
    }
    ++cnt;
  }

  putchar('\n');
  // printf("\tDBG: chk count = %u\t%f\n", cnt, (double)cnt / sets.nTrigs);

  board1->AO.AO_Timer.Status_1_Register.refresh(&rlpStatus);
  if (!board1->AO.AO_Timer.Status_1_Register.readBC_TC_St(&rlpStatus)) {
    eprError |= 1 << 22;
  }

  ao1Stream->disable(rlpStatus);
  ao1DMA->stop(rlpStatus);

//==============================================================================
//	Finalize data aquisition

  board0->AI.AI_Timer.Command_Register.writeEnd_On_SC_TC(kTrue, &rlpStatus);

  rlpTime = 0;
  rlpStart = clock();

  while (board0->AI.AI_Timer.Status_1_Register.readSC_Armed_St(&rlpStatus)) {
    if (rlpTime > RLP_TIMEOUT) {
      printf("\t\e[1;35mWarning:\e[0m AI timing engine "
	     "did not stop within timeout\n");
      break;
    }

    rlpTime = static_cast<double>(clock() - rlpStart) / CLOCKS_PER_SEC;
  }

  rlpTime = 0;
  rlpStart = clock();

  while (!ai0Stream->fifoIsEmpty(rlpStatus)) {
    if (rlpTime > RLP_TIMEOUT) {
      printf("\t\e[1;35mWarning:\e[0m AI stream circuit "
	     "did not flush within timeout\n");
      break;
    }

    rlpTime = static_cast<double>(clock() - rlpStart) / CLOCKS_PER_SEC;
  }

  ai0Stream->disable(rlpStatus);
  ai0DMA->stop(rlpStatus);

//==============================================================================
//	Read remaining data from DMA

  ai0DMA->read(0, NULL, &bytesAvailable, kFalse, &dataOverwritten, rlpStatus);

  if (rlpStatus.isFatal()) {
    eprError |= 1 << 5;
  }

  while (bytesAvailable >= singleScanBytes) {
    ai0DMA->read(singleScanBytes,
		 reinterpret_cast<unsigned char *>(&rawADC_Data[0]),
		 &bytesAvailable, kFalse, &dataOverwritten, rlpStatus);

    if (rlpStatus.isFatal()) {
      eprError |= 1 << 5;
      break;
    }

    if (scanCounter == sets.nTrigs) {
      eprError |= 1 << 6;
      break;
    }

    nNISTC3::nAIDataHelper::scaleData(rawADC_Data, sets.n, adcDataVolt, sets.n,
				      ai0Config, *eeprom0, *board0_Info);

    std::copy_n(adcDataVolt.begin(), sets.n,
		eprData.begin() + sets.n * scanCounter);

    ++scanCounter;
  }

  if (scanCounter != sets.nTrigs) {
    eprError |= 1 << 6;
  }

  if (bytesAvailable) {
    printf("\t\e[1;35mWarning:\e[0m incomplete data receieved\n");
    printf("\tData left in DMA buffer: %u bytes\n", bytesAvailable);
  }

//==============================================================================
//	Report errors if any

  if (eprError & (1 << 5)) {
    printf("\t\e[1;31mRuntime error:\e[0m unable to read AI DMA buffer\n");
  }

  if (eprError & (1 << 6)) {
    printf("\t\e[1;31mRuntime error:\e[0m AI trigger failure\n");
  }

  if (eprError & (1 << 1)) {
    printf("\t\e[1;31mRuntime error:\e[0m AI sample clock overrun\n");
  }

  if (eprError & (1 << 2)) {
    printf("\t\e[1;31mRuntime error:\e[0m ADC overrun\n");
  }

  if (eprError & (1 << 3)) {
    printf("\t\e[1;31mRuntime error:\e[0m AI FIFO overflow\n");
  }

  if (eprError & (1 << 9)) {
    printf("\t\e[1;31mRuntime error:\e[0m field sweep FIFO underflow\n");
  }

  if (eprError & (1 << 10)) {
    printf("\t\e[1;31mRuntime error:\e[0m field sweep DAC overrun\n");
  }

  if (eprError & (1 << 17)) {
    printf("\t\e[1;31mRuntime error:\e[0m gradients FIFO underflow\n");
  }

  if (eprError & (1 << 18)) {
    printf("\t\e[1;31mRuntime error:\e[0m gradients DAC overrun\n");
  }

  if (eprError & (1 << 22)) {
    printf("\t\e[1;31mRuntime error:\e[0m gradients did not stop\n");
  }

//==============================================================================
//	Print summary

  time(&end);
  elapsedTime = end - start;

  printf("\tData acquisition done. Elapsed time %02li:%02li\n",
	 elapsedTime / 60, elapsedTime % 60);

  printf("\tRecorded %lu (%lu) projections\n",
	 scanCounter, sets.k3 * sets.nScans);

  return;
}

//==============================================================================
//	Record a spectrum, rearm timing circuitries

void eprInstrument::retriggerableScan(std::vector<float> &eprData,
				EPR::settings &sets,
				EPR::status_t &eprStatus)
{
  if (eprStatus != EPR::idle) {
    return;
  }

  size_t scanCounter = 0;
  size_t singleScanBytes = sets.n * sizeof(short);
  unsigned int bytesAvailable = 0;
  unsigned int dataOverwritten = kFalse;

  std::vector<short> rawADC_Data(sets.n, 0);
  std::vector<float> adcDataVolt(sets.n, 0);
  eprData.resize(sets.n * sets.nTrigs);

  unsigned int eprError = 0;

  double rlpTime;
  clock_t rlpStart;

//==============================================================================
//	Start field sweep and data aquisition

  board0->AO.AO_Timer.Command_1_Register.writeSTART1_Pulse(kTrue, &rlpStatus);

  while (!board0->AO.AO_Timer.Status_1_Register.readBC_TC_St(&rlpStatus)) {
    ai0DMA->read(0, NULL, &bytesAvailable, kFalse, &dataOverwritten, rlpStatus);

    if (rlpStatus.isFatal()) {
      eprError |= 1 << 5;
      break;
    }

    if (bytesAvailable >= singleScanBytes) {
      ai0DMA->read(singleScanBytes,
		   reinterpret_cast<unsigned char *>(&rawADC_Data[0]),
		   &bytesAvailable, kFalse, &dataOverwritten, rlpStatus);

      if (rlpStatus.isFatal()) {
	eprError |= 1 << 5;
	break;
      }

      if (scanCounter == sets.nTrigs) {
	eprError |= 1 << 6;
	break;
      }

      ai0Stream->modifyTransferSize(singleScanBytes, rlpStatus);

      nNISTC3::nAIDataHelper::scaleData(rawADC_Data, sets.n,
					adcDataVolt, sets.n,
					ai0Config, *eeprom0, *board0_Info);

      std::copy_n(adcDataVolt.begin(), sets.n,
		  eprData.begin() + sets.n * scanCounter);

      ++scanCounter;
    }

    // Check AI errors
    board0->AI.AI_Timer.Status_1_Register.refresh(&rlpStatus);

    if (board0->AI.AI_Timer.Status_1_Register.getScanOverrun_St(&rlpStatus)) {
	eprError |= 1 << 1;
	break;
    }

    if (board0->AI.AI_Timer.Status_1_Register.getOverrun_St(&rlpStatus)) {
	eprError |= 1 << 2;
	break;
    }

    if (board0_Info->isSimultaneous && board0->SimultaneousControl.\
	InterruptStatus.readAiFifoOverflowInterruptCondition(&rlpStatus)) {
	eprError |= 1 << 3;
	break;
    }

    if (!board0_Info->isSimultaneous && board0->AI.AI_Timer.\
	Status_1_Register.getOverflow_St(&rlpStatus)) {
	eprError |= 1 << 3;
	break;
    }

    // Check board0 AO errors
    board0->AO.AO_Timer.Status_1_Register.refresh(&rlpStatus);

    if (board0->AO.AO_Timer.Status_1_Register.getUnderflow_St(&rlpStatus)) {
	eprError |= 1 << 9;
	break;
    }

    if (board0->AO.AO_Timer.Status_1_Register.getOverrun_St(&rlpStatus)) {
	eprError |= 1 << 10;
	break;
    }
  }

//==============================================================================
//	Finalize data aquisition

  board0->AI.AI_Timer.Command_Register.writeEnd_On_SC_TC(kTrue, &rlpStatus);

  rlpTime = 0;
  rlpStart = clock();

  while (board0->AI.AI_Timer.Status_1_Register.readSC_Armed_St(&rlpStatus)) {
    if (rlpTime > RLP_TIMEOUT) {
      eprStatus = EPR::rlperror;
      break;
    }

    rlpTime = static_cast<double>(clock() - rlpStart) / CLOCKS_PER_SEC;
  }

  rlpTime = 0;
  rlpStart = clock();

  while (!ai0Stream->fifoIsEmpty(rlpStatus)) {
    if (rlpTime > RLP_TIMEOUT) {
      eprStatus = EPR::rlperror;
      break;
    }

    rlpTime = static_cast<double>(clock() - rlpStart) / CLOCKS_PER_SEC;
  }

//==============================================================================
//	Read remaining data from DMA

  rlpTime = 0;
  rlpStart = clock();

  do {		// Stream circuit is enabled, data may continue to arrive
    ai0DMA->read(0, NULL, &bytesAvailable, kFalse, &dataOverwritten, rlpStatus);

    if (rlpStatus.isFatal()) {
      eprError |= 1 << 5;
      break;
    }

    if (bytesAvailable >= singleScanBytes) {
      ai0DMA->read(singleScanBytes,
		   reinterpret_cast<unsigned char *>(&rawADC_Data[0]),
		   &bytesAvailable, kFalse, &dataOverwritten, rlpStatus);

      if (rlpStatus.isFatal()) {
	eprError |= 1 << 5;
	break;
      }

      if (scanCounter == sets.nTrigs) {
	eprError |= 1 << 6;
	break;
      }

      ai0Stream->modifyTransferSize(singleScanBytes, rlpStatus);

      nNISTC3::nAIDataHelper::scaleData(rawADC_Data, sets.n,
					adcDataVolt, sets.n,
					ai0Config, *eeprom0, *board0_Info);

      std::copy_n(adcDataVolt.begin(), sets.n,
		  eprData.begin() + sets.n * scanCounter);

      ++scanCounter;
    }

    if (rlpTime > RLP_TIMEOUT) {
      eprStatus = EPR::rlperror;
      break;
    }

    rlpTime = static_cast<double>(clock() - rlpStart) / CLOCKS_PER_SEC;
  } while (bytesAvailable);

  if (scanCounter != sets.nTrigs) {
    eprError |= 1 << 6;
  }

  if (eprError) {
    eprStatus = EPR::rlperror;
    return;
  }

//==============================================================================
//	Rearm timing circuitries

  // clear AI status registers and acknowledge interruptions
  board0->AI.AI_Timer.Interrupt2_Register.writeRegister(0xFFFFFFFF, &rlpStatus);

  // NI bug workaround, disarm DIV
  board0->AI.AI_Timer.Command_Register.writeDisarm(1, &rlpStatus);

  rlpTime = 0;
  rlpStart = clock();

  while (board0->AI.AI_Timer.Status_1_Register.readDIV_Armed_St(&rlpStatus)) {
    if (rlpTime > RLP_TIMEOUT) {
      eprStatus = EPR::rlperror;
      break;
    }

    rlpTime = static_cast<double>(clock() - rlpStart) / CLOCKS_PER_SEC;
  }

  ai0Helper->getInTimerHelper(rlpStatus).armTiming(inTimingConfig, rlpStatus);

  // clear AO status registers and acknowledge interruptions
  board0->AO.AO_Timer.Interrupt2_Register.writeRegister(0xFFFFFFFF,
							&rlpStatus);

  ao0Helper->getOutTimerHelper(rlpStatus).setArmUI(kTrue, rlpStatus);
  ao0Helper->getOutTimerHelper(rlpStatus).setArmUC(kTrue, rlpStatus);
  ao0Helper->getOutTimerHelper(rlpStatus).setArmBC(kTrue, rlpStatus);
  ao0Helper->getOutTimerHelper(rlpStatus).armTiming(rlpStatus);

  if (rlpStatus.isFatal()) {
    eprStatus = EPR::rlperror;
  }

  return;
}

//==============================================================================
//	Disarm timings, zero AOs, disable stream circuits and DMA channels

void eprInstrument::disarm(EPR::status_t &eprStatus)
{
  if (eprStatus == EPR::offline) {
    return;
  }

  // helpers won't disarm timers if status is fatal
  rlpStatus.statusCode = kStatusSuccess;

  if (board0->AI.AI_Timer.Status_1_Register.readSC_Armed_St(&rlpStatus)) {
    ai0Helper->getInTimerHelper(rlpStatus).disarmTiming(rlpStatus);
  }

  if (board0->AO.AO_Timer.Status_1_Register.readUC_Armed_St(&rlpStatus)) {
    ao0Helper->getOutTimerHelper(rlpStatus).disarmTiming(rlpStatus);
  }

  if (board1->AO.AO_Timer.Status_1_Register.readUC_Armed_St(&rlpStatus)) {
    ao1Helper->getOutTimerHelper(rlpStatus).disarmTiming(rlpStatus);
  }

  ai0Stream->disable(rlpStatus);
  ao0Stream->disable(rlpStatus);
  ao1Stream->disable(rlpStatus);

  ai0DMA->stop(rlpStatus);
  ao0DMA->stop(rlpStatus);
  ao1DMA->stop(rlpStatus);

  ai0Helper->reset(rlpStatus);
  ao0Helper->reset(rlpStatus);
  ao1Helper->reset(rlpStatus);

  board0->AO.AO_Direct_Data[0].writeRegister(zerosDAC0[0], &rlpStatus);
  board0->AO.AO_Direct_Data[1].writeRegister(zerosDAC0[1], &rlpStatus);

  // center field in AO_Direct_Data[0] should not be touched
  board1->AO.AO_Direct_Data[1].writeRegister(zerosDAC1[1], &rlpStatus);
  board1->AO.AO_Direct_Data[2].writeRegister(zerosDAC1[2], &rlpStatus);
  board1->AO.AO_Direct_Data[3].writeRegister(zerosDAC1[3], &rlpStatus);

  if (rlpStatus.isFatal()) {
    eprStatus = EPR::rlperror;
  }

  return;
}

eprInstrument::~eprInstrument(void)
{
  if (ai0Stream) delete ai0Stream;
  if (ao0Stream) delete ao0Stream;
  if (ao1Stream) delete ao1Stream;

  if (ai0DMA) delete ai0DMA;
  if (ao0DMA) delete ao0DMA;
  if (ao1DMA) delete ao1DMA;

  if (ai0Helper) delete ai0Helper;
  if (ao0Helper) delete ao0Helper;
  if (ao1Helper) delete ao1Helper;

  if (eeprom0) delete eeprom0;
  if (eeprom1) delete eeprom1;

  if (board0) delete board0;
  if (board1) delete board1;

  if (bus[0]) releaseBoard(bus[0]);
  if (bus[1]) releaseBoard(bus[1]);

  return;
}
