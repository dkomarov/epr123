// frqCounter.cpp
//
// Frequency counter
//

#include <termios.h>

#include "eprTypes.h"

class frqCounter
{
public:
  frqCounter(void);

  void open(void);

  void configure(void);

  int isAvailable(void);

  void flush(void);

  double getFrequency(void);

  ~frqCounter(void);

private:
  char ttyUSB[MAX_LENGTH];
  int fileDsc;

  struct termios oldtio, newtio;

  int devAvailable = 0;
};
