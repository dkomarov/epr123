// gradients.h
//
// Generates XYZ gradients with several types of distribution,
// optional random shift and different sorting methods
//

#include <cmath>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <algorithm>
#include <numeric>
#include <random>

#include "gradients.h"

#define PCK_DENSITY (0.7404804896930609)	// M_PI / 3.0 / sqrt(2.0)
#define SQRT63 (0.8164965809277259)		// sqrt(6.0) / 3.0
#define SQRT32 (0.8660254037844386)		// sqrt(3.0) / 2.0
#define FWHM (2.3548200450309493)		// Gaussian width
#define GM_SEED1 (0.4655712318767679)		// Golden mean ratio
#define GM_SEED2 (0.6823278038280193)		// ... two-dimensional

//==============================================================================
//	SPI cubic gradients

static void spiCubic(std::vector<double> &xyz, EPR::settings &sets)
{
  long int k, parity, first, last;
  long int ii, ix, iy, iz;
  int xDirection, yDirection;

  k = round(cbrt(sets.k3));

  if (k * k * k != (long int)sets.k3) {
    sets.k3 = k * k * k;
    sets.nCycles = sets.k3 * sets.nScans / 2 + 1;	// integer division OK
    sets.nTrigs = sets.nCycles * 2;

    printf("\t\e[1;35mWarning:\e[0m number of projections for SPI gradients "
	   "is expected to be k^3\n");
    printf("\tGenerating SPI cubic gradients with k = %ld (k^3 = %lu)\n",
	   k, sets.k3);
  }

  xyz.resize(sets.k3 * 3);

  parity = (k % 2);
  first = -(k / 2);			// integer division OK
  last = (k - 1) / 2;
  xDirection = 0;
  yDirection = 0;

  ii = 0;

  for (iz = first; iz  <= last; ++iz) {
    for (iy = first; iy  <= last; ++iy) {
      for (ix = first; ix  <= last; ++ix) {

	if (xDirection) {
	  xyz[ii] = parity - 1 - ix;
	}
	else {
	  xyz[ii] = ix;
	}

	if (yDirection) {
	  xyz[sets.k3 + ii] = parity - 1 - iy;
	}
	else {
	  xyz[sets.k3 + ii] = iy;
	}

	xyz[sets.k3 * 2 + ii] = iz;

	++ii;
      }
      xDirection = !xDirection;
    }
    yDirection = !yDirection;
  }

  for (ii = 0; ii < (long int)sets.k3 * 3; ++ii) {
    xyz[ii] *= sets.maxGr / (k / 2);	// integer division OK
  }

  if (sets.grOrder != EPR::snake) {
    printf("\t\e[1;35mWarning:\e[0m sorting methods other than \"snake\" "
	   "have not been implemented yet\n");
    sets.grOrder = EPR::snake;
  }

  if (sets.dither > 0.0) {
    printf("\t\e[1;35mWarning:\e[0m dithering is not available "
	   "for SPI gradients\n");
    sets.dither = 0.0;
  }

  return;
}

//==============================================================================
//	HCP lattice gradients

static void hcpLattice(std::vector<double> &xyz, EPR::settings &sets)
{
  size_t k3, k3_, ii;
  long int x_nodes, ix;
  long int y_rows, iy;
  long int z_layers, iz;;

  int xDirection = 1;
  int yDirection = 1;

  double r_plate, r_max;
  long int ir;

  k3 = sets.k3;
  xyz.resize(k3 * 3);

  r_max = cbrt(k3 / PCK_DENSITY / 8.0) + sets.dither / FWHM;
  x_nodes = round(r_max + 0.1);	// checked emperically
  y_rows = round(r_max / SQRT32);	// up to 1M gradients
  z_layers = round(r_max / SQRT63);
  k3_ = (x_nodes * 2 + 1) * (y_rows * 2 + 1) * (z_layers * 2 + 1);

  std::vector<double> x(k3_, 0.0);
  std::vector<double> y(k3_, 0.0);
  std::vector<double> z(k3_, 0.0);

  std::vector<double> r2(k3_, 0.0);
  std::vector<size_t> order(k3_, 0);
  std::vector<double> phi(k3, 0);

  ii = 0;
  for (iz = -z_layers; iz <= z_layers; ++iz) {
    for (iy = -y_rows; iy <= y_rows; ++iy) {
      for (ix = -x_nodes; ix <= x_nodes; ++ix) {

	z[ii] = iz * SQRT63;
	y[ii] = iy * SQRT32 * yDirection + (iz & 1) * SQRT32 / 3.0;
	x[ii] = ix * xDirection + ((iy & 1) - (iz & 1)) * 0.5;

	++ii;
      }
      xDirection *= -1;
    }
    yDirection *= -1;
  }

  std::random_device rd;
  std::mt19937 mt(rd());

  if (sets.dither > 0) {	// add artificial noise to the gradients
    std::normal_distribution<double> qt(0, sets.dither / FWHM);

    for (ii = 0; ii < k3_; ++ii) {	// preserve zero-gradient
      if (ii == k3_ / 2) {		// integer division OK
	continue;
      }

      x[ii] += qt(mt);
      y[ii] += qt(mt);
      z[ii] += qt(mt);
    }
  }

  for (ii = 0; ii < k3_; ++ii) {
    r2[ii] = x[ii] * x[ii] + y[ii] * y[ii] + z[ii] * z[ii];
  }

  std::iota(order.begin(), order.end(), 0);
  std::sort(order.begin(), order.end(),
	    [&r2](size_t i1, size_t i2) {return r2[i1] < r2[i2];});

  order.resize(k3);

  r_max = sqrt(r2[order.back()]);
  if (!r_max) {
    r_max = 1.0;		// to avoid division by zero
  }

  if (sets.grOrder == EPR::ascend) {
    // nothing to do here
  }
  else if (sets.grOrder == EPR::descend) {
    std::reverse(order.begin(), order.end());
  }
  else if (sets.grOrder == EPR::spiral) {
    std::vector<double> phi(k3_, 0.0);

    for (ii = 0; ii < k3; ++ii) {
      iz = round(z[order[ii]] / SQRT63);
      ir = round(sqrt(x[order[ii]] * x[order[ii]] +
		      y[order[ii]] * y[order[ii]]));

      if (fabs(iz) * SQRT63 > r_max) {
	r_plate = 0;
      }
      else {
	r_plate = sqrt(r_max * r_max - iz * iz * 2.0 / 3.0);
      }

      if (ir > floor(r_plate - 0.5)) {
	ir = floor(r_plate - 0.5);
	if (ir < 0) ir = 0;
      }

      if (iz & 1) {
	ir = x_nodes - ir;
      }

      phi[order[ii]] = atan2(y[order[ii]], x[order[ii]]) +
	(iz * (x_nodes + 1) + ir) * 2 * M_PI;
    }

    std::sort(order.begin(), order.end(),
	      [&phi](size_t i1, size_t i2) {return phi[i1] < phi[i2];});
  }
  else if (sets.grOrder == EPR::random) {
    std::shuffle(order.begin(), order.end(), mt);
  }
  else if(sets.grOrder ==  EPR::snake) {
    std::sort(order.begin(), order.end());
  }

  for (ii = 0; ii < k3; ++ii) {
    xyz[ii] = x[order[ii]] / r_max * sets.maxGr;
    xyz[k3 + ii] = y[order[ii]] / r_max * sets.maxGr;
    xyz[k3 * 2 + ii] = z[order[ii]] / r_max * sets.maxGr;
  }

  return;
}

//==============================================================================
//	GM3D spherical gradients

void gm3dSphere(std::vector<double> &xyz, EPR::settings &sets)
{
  size_t k3, i0, ii;
  double xy, unused, nCirc;

  k3 = sets.k3;
  xyz.resize(k3 * 3);

  std::vector<double> x(k3, 0.0);
  std::vector<double> y(k3, 0.0);
  std::vector<double> z(k3, 0.0);
  std::vector<double> phi(k3, 0);

  std::vector<size_t> order(k3, 0);
  std::iota(order.begin(), order.end(), 0);

  i0 = 1;
  if (sets.dither > 0) {	// randomize starting point
    std::random_device rd;
    i0 += rd() % k3;
  }

  for (ii = 0; ii < k3; ++ii) {
    phi[ii] = modf(GM_SEED1 * (i0 + ii), &unused) * 2.0 * M_PI;
    z[ii] = modf(GM_SEED2 * (i0 + ii), &unused) * 2.0 - 1.0;

    xy = sqrt(1.0 - z[ii] * z[ii]);
    x[ii] = xy * cos(phi[ii]);
    y[ii] = xy * sin(phi[ii]);
  }

  if (sets.grOrder == EPR::ascend) {
    printf("\t\e[1;35mWarning:\e[0m sorting method \"ascend\" "
	   "is not defined for spheric gradients\n");
    printf("\tGradients are sorted in default \"random\" order\n");

    sets.grOrder = EPR::random;
  }
  else if (sets.grOrder == EPR::descend) {
    printf("\t\e[1;35mWarning:\e[0m sorting method \"descend\" "
	   "is not defined for spheric gradients\n");
    printf("\tGradients are sorted in default \"random\" order\n");

    sets.grOrder = EPR::random;
  }
  else if (sets.grOrder == EPR::spiral) {
    nCirc = round(sqrt(k3) / 2.0) / 2.0;	// nCirc / 2.0

    for (ii = 0; ii < k3; ++ii) {
      phi[ii] += 2.0 * M_PI * floor((z[ii] + 1.0) * nCirc);
    }

    std::sort(order.begin(), order.end(),
	      [&phi](size_t i1, size_t i2) {return phi[i1] < phi[i2];});
  }
  else if (sets.grOrder == EPR::random) {
    // nothing to do here
  }
  else if (sets.grOrder == EPR::snake) {
    printf("\t\e[1;35mWarning:\e[0m sorting method \"snake\" "
	   "is not defined for GM3D gradients\n");
    printf("\tGradients are sorted by default \"random\" order\n");

    sets.grOrder = EPR::random;
  }

  for (ii = 0; ii < k3; ++ii) {
    xyz[ii] = x[order[ii]] * sets.maxGr;
    xyz[k3 + ii] = y[order[ii]] * sets.maxGr;
    xyz[k3 * 2 + ii] = z[order[ii]] * sets.maxGr;
  }

  return;
}

//==============================================================================
//	Custom gradienrs

int customGradients(std::vector<double> &xyz, EPR::settings &sets)
{
  FILE *file;
  size_t sz;

  file = fopen(sets.grFile, "r");
  if (file == NULL) {
    return 0;
  }

  fseek(file, 0, SEEK_END);
  sz = ftell(file);

  if (!sz || sz % (sizeof(double) * 3)) {
    fclose(file);
    return 0;
  }

  sets.k3 = sz / sizeof(double) / 3;
  sets.nCycles = sets.k3 * sets.nScans / 2 + 1;	// integer division OK
  sets.nTrigs = sets.nCycles * 2;

  xyz.resize(sets.k3 * 3);

  fseek(file, 0, SEEK_SET);
  fread(xyz.data(), sizeof(double), sets.k3 * 3, file);

  fclose(file);

  return 1;
}

//==============================================================================
//	Method selector

void generateGradients(std::vector<double> &xyz, EPR::settings &sets)
{
  switch (sets.grDistr) {
  case EPR::spi:
    spiCubic(xyz, sets);
    break;

  case EPR::hcp:
    hcpLattice(xyz, sets);
    break;

  case EPR::gm3d:
    gm3dSphere(xyz, sets);
    break;

  case EPR::custom:
    if (!customGradients(xyz, sets)) {
      printf("\t\e[1;35mWarning:\e[0m file \"%s\" is missing or corrupted\n",
	     sets.grFile);
      printf("\tGenerating GM3D spherical gradients instead\n");

      sets.grDistr = EPR::gm3d;
      sets.grOrder = EPR::spiral;
      gm3dSphere(xyz, sets);
    }

    break;
  }

  return;
}
