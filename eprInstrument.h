// eprInstrument.h
//
// EPR instrument
//

#ifndef eprInstrument_h
#define eprInstrument_h

#include "osiBus.h"

#include "devices.h"
#include "dataHelper.h"
#include "eepromHelper.h"
#include "inTimer/aiHelper.h"
#include "outTimer/aoHelper.h"
#include "simultaneousInit.h"
#include "streamHelper.h"
#include "tXSeries.h"
#include "CHInCh/tCHInChDMAChannel.h"

#include "eprTypes.h"

#define DMA_FACTOR 25		// DMA buffer factor
#define TB3 100.0e6		// DAQ board internal 100 MHz timebase
#define RLP_TIMEOUT 3

class eprInstrument
{
public:
  eprInstrument(EPR::calibrations cals, EPR::status_t &eprStatus);

  void initialize(EPR::status_t &eprStatus);

  void updateConfiguration(EPR::settings &sets, EPR::status_t &eprStatus);

  void setCenterField(double cfGauss, EPR::status_t &eprStatus);

  void prepAcquisition(EPR::settings &sets, EPR::status_t &eprStatus);

  void primeFieldSweep(std::vector<double> &swDataGauss,
		       std::vector<float> &syncSignal,
		       EPR::status_t &eprStatus);

  void armGradients(std::vector<double> &xyzGauss1cm,
		    EPR::settings &sets,
		    EPR::status_t &eprStatus);

  void runAcquisition(std::vector<float> &eprData,
		      EPR::settings &sets,
		      time_t &start, time_t &end);

  void retriggerableScan(std::vector<float> &eprData,
			 EPR::settings &sets,
			 EPR::status_t &eprStatus);

  void disarm(EPR::status_t &eprStatus);

  ~eprInstrument(void);

private:
  const EPR::calibrations cals;

  iBus *bus[2] = {NULL};

  nMDBG::tStatus2 rlpStatus;

  tXSeries *board0 = NULL;
  tXSeries *board1 = NULL;

  const nNISTC3::tDeviceInfo *board0_Info, *board1_Info;

  nNISTC3::eepromHelper *eeprom0 = NULL;
  nNISTC3::eepromHelper *eeprom1 = NULL;

  nNISTC3::aiHelper *ai0Helper = NULL;
  nNISTC3::aoHelper *ao0Helper = NULL;
  nNISTC3::aoHelper *ao1Helper = NULL;

  nNISTC3::streamHelper *ai0Stream = NULL;
  nNISTC3::streamHelper *ao0Stream = NULL;
  nNISTC3::streamHelper *ao1Stream = NULL;

  nNISTC3::tCHInChDMAChannel *ai0DMA = NULL;
  nNISTC3::tCHInChDMAChannel *ao0DMA = NULL;
  nNISTC3::tCHInChDMAChannel *ao1DMA = NULL;

  nNISTC3::tInputRange ai0Range;
  const nNISTC3::tOutputRange ao0Range = nNISTC3::kOutput_10V;
  const nNISTC3::tOutputRange ao1Range = nNISTC3::kOutput_10V;

  double sampleRateMax;

  unsigned short ai0Gain;
  unsigned char ao0Gain, ao1Gain;

  std::vector<short> zerosDAC0 = std::vector<short>(2, 0);
  std::vector<short> zerosDAC1 = std::vector<short>(4, 0);

  std::vector<nNISTC3::aiHelper::tChannelConfiguration>
  ai0Config = std::vector<nNISTC3::aiHelper::tChannelConfiguration>(1);

  std::vector<nNISTC3::aoHelper::tChannelConfiguration>
  ao0Config = std::vector<nNISTC3::aoHelper::tChannelConfiguration>(2);

  std::vector<nNISTC3::aoHelper::tChannelConfiguration>
  ao1Config = std::vector<nNISTC3::aoHelper::tChannelConfiguration>(4);

  nNISTC3::inTimerParams inTimingConfig;

  int chkFieldSweep(std::vector<double> &swDataGauss);

  int chkGradients(std::vector<double> &xyzGauss1cm);

  void scaleGradients(std::vector<float> &xyzVolt,
		      std::vector<double> &xyzGauss1cm,
		      EPR::settings &sets);

  unsigned int cnt;		// DBG purposes
  unsigned int reg;		// DBG purposes
};

#endif	// eprInstrument_h
