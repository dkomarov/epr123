// fieldSweep.cpp
//
// Loads sweep calibration data from file and generates field sweep profile
//

#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>

#include "fieldSweep.h"

// Gaussian relaxation factor for sweep harmonics
#define RLX_FACTOR 43.3	// ~3 ppm deviation with sweep overhead 5%

//==============================================================================

static int loadSwCalib(double swTime, double *amplitude, double *phase)
{
  FILE *file;
  char fname[MAX_LENGTH];
  char str[MAX_LENGTH];

  double amp, phi;
  size_t no, hmCount = 0;

  sprintf(fname, "%s%gs", SW_CALIB_DIR, swTime);

  file = fopen(fname, "r");
  if (file == NULL) {
    return 0;
  }

  while (fgets(str, MAX_LENGTH, file)) {
    if (*str == '#' || *str == '\n') {
      continue;
    }
    else if (sscanf(str, "%lu : %lf : %lf", &no, &amp, &phi) == 3) {
      if (no < HRM_NUMBER) {
	*(amplitude + no) = amp;
	*(phase + no) = phi * M_PI / 180.0;
	++hmCount;
      }
      else {
	printf("\t\e[1;35mWarning:\e[0m in the file \"%s\"\n"
	       "\tHarmonic number is larger than maximum allowed %u\n",
	       fname, HRM_NUMBER - 1);
      }
    }
  }

  fclose(file);

  return hmCount > 0 ? 1 : 0;
}

//==============================================================================

static int interpolateSwHrms(double swTime, double *amplitude, double *phase)
{
  FILE *file;
  char str[MAX_LENGTH];

  double frequency[HRM_NUMBER];
  double attenuation[HRM_NUMBER];
  double delay[HRM_NUMBER];

  size_t ih, j;
  double frq;

  size_t frqCount = 0;

  file = fopen(SINEWAVE_RSP, "r");
  if (file == NULL) {
    return 0;
  }

  while (fgets(str, MAX_LENGTH, file) && frqCount < HRM_NUMBER) {
    if (*str == '#' || *str == '\n') {
      continue;
    }
    else if (sscanf(str, "%lf : %lf : %lf", frequency + frqCount,
		    attenuation + frqCount, delay + frqCount) == 3) {
      ++frqCount;
    }
  }

  fclose(file);

  if (frqCount < 2 || *frequency != 0.0) {
    return 0;
  }		// TODO: chk for ascend order

  for (ih = 1; ih < HRM_NUMBER; ih += 2) {
    frq = ih / (swTime * 2.0);

    if (frq > frequency[frqCount - 1]) {
      break;
    }

    j = 0;
    while (frq > frequency[++j]);

    amplitude[ih] = (frequency[j] - frequency[j - 1]) /
      (attenuation[j] * (frq - frequency[j - 1]) +
       attenuation[j - 1] * (frequency[j] - frq)) *
      exp(-0.5 * ih * ih / (RLX_FACTOR * RLX_FACTOR));

    phase[ih] = 2 * M_PI * frq / (frequency[j] - frequency[j - 1])
      * (delay[j] * (frq - frequency[j - 1])
	 + delay[j - 1] * (frequency[j] - frq));
  }

  return 1;
}

//==============================================================================

void generateTriangHrms(double *amplitude, double *phase)
{
  size_t ih;

  for (ih = 1; ih < HRM_NUMBER; ih += 2) {
    amplitude[ih] = 1.0;
    phase[ih] = 0.0;
  }

  return;
}

//==============================================================================

void generateFieldSweep(std::vector<double> &swDataGauss,
			std::vector<float> &syncSignal,
			EPR::settings &sets)
{
  size_t ii, ih;
  size_t n_2 = sets.n_ * 2;

  double wt;
  double amplitude[HRM_NUMBER] = {0};
  double phase[HRM_NUMBER] = {0};

  swDataGauss.assign(n_2, 0);
  syncSignal.assign(n_2, 0);

  if (!loadSwCalib(sets.swTime, amplitude, phase)) {
      printf("\t\e[1;35mWarning:\e[0m unable to find "
	     "valid calibration data for sweep %g s\n", sets.swTime);
      printf("\tGenerating sweep profile using "
	     "magnet frequency response function\n");

      if (!interpolateSwHrms(sets.swTime, amplitude, phase)) {
	printf("\t\e[1;31mError:\e[0m cannot read file \"%s\"\n", SINEWAVE_RSP);
	printf("\tGenerating triangular sweep profile, "
	       "which is likely to be inaccurate\n");

	generateTriangHrms(amplitude, phase);
      }
  }

  for (ii = 0; ii < n_2; ++ii) {
    wt = ((double)ii / sets.n_ + sets.syncDelay / sets.swTime) * M_PI;

    for (ih = 0; ih < HRM_NUMBER; ++ih) {
      if (!amplitude[ih]) {
	continue;
      }

      swDataGauss[ii] += amplitude[ih] * cos(ih * wt + phase[ih]) / ih / ih;
    }

    swDataGauss[ii] *= sets.swAdjusted * 4.0 / M_PI / M_PI;

    if (ii * 2 < sets.trigPulse || ii * 2 > n_2 * 2 - sets.trigPulse) {
      syncSignal[ii] = SYNC_HIGH;
    }
    else if (ii * 2 > n_2 - sets.trigPulse && ii * 2 < n_2 + sets.trigPulse) {
      syncSignal[ii] = SYNC_HIGH;
    }
  }

  ii = 0;
  while (swDataGauss[ii] < 0 || swDataGauss[++ii] >= 0);	// checked, -> 0

  std::rotate(swDataGauss.begin(),
	      swDataGauss.begin() + ii, swDataGauss.end());

  std::rotate(syncSignal.begin(), syncSignal.begin() + ii, syncSignal.end());

  return;
}
