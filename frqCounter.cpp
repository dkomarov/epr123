// frqCounter.cpp
//
// Frequency counter
//

#include <cstdio>
#include <cstring>

#include <fcntl.h>
#include <unistd.h>

#include "frqCounter.h"

#define TC_DELAY 33000		// serial port buffer flush delay, us
#define TC_TIMEOUT 5		// 500 ms timeout, if no data received
#define PKG_LENGTH 18		// package structure: "    1234567890E0\r\n"

//==============================================================================

frqCounter::frqCounter(void)
{
  FILE *file;
  char str[MAX_LENGTH];

  *ttyUSB = 0;

  file = fopen(FRQ_COUNTER, "r");
  if (file == NULL) {
    printf("\t\e[1;35mWarning:\e[0m cannot open file \"%s\"\n", FRQ_COUNTER);
    return;
  }

  while (fgets(str, MAX_LENGTH, file)) {
    if (*str == '#' || *str == '\n') {
      continue;
    }
    else if (!strncmp(str, "/dev/", 5)) {
      str[strcspn(str, "\n")] = 0;
      strcpy(ttyUSB, str);
      break;
    }
  }

  fclose(file);

  if (*ttyUSB == 0) {
    printf("\t\e[1;35mWarning:\e[0m file \"%s\" is corrupted\n", FRQ_COUNTER);
  }

  return;
}

//==============================================================================

void frqCounter::open(void)
{
  fileDsc = -1;

  if (*ttyUSB == 0) {
    return;
  }

  fileDsc = ::open(ttyUSB, O_RDWR | O_NOCTTY);
  if (fileDsc < 0) {
    printf("\t\e[1;35mWarning:\e[0m cannot open USB device \"%s\"\n", ttyUSB);
    return;
  }

  tcgetattr(fileDsc, &oldtio);

  bzero(&newtio, sizeof(newtio));
  newtio.c_cflag = B38400 | CRTSCTS | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR;
  newtio.c_oflag = 0;

  newtio.c_lflag = 0;

  newtio.c_cc[VTIME] = TC_TIMEOUT;
  newtio.c_cc[VMIN] = 0;

  tcflush(fileDsc, TCIFLUSH);
  tcsetattr(fileDsc,TCSANOW,&newtio);

  return;
}

//==============================================================================

void frqCounter::configure(void)
{
  FILE *file;
  char str[MAX_LENGTH];
  long int len;
  int fail = 0;

  if (fileDsc < 0) {
    return;
  }

  file = fopen(FRQ_COUNTER, "r");
  if (file == NULL) {
    printf("\t\e[1;35mWarning:\e[0m cannot open file \"%s\"\n", FRQ_COUNTER);
    return;
  }

  while (fgets(str, MAX_LENGTH, file)) {
    if (*str == '#' || *str == '\n' || *str == '/') {
      continue;
    }

    len = strlen(str);
    if (write(fileDsc, str, len) < len) {
      printf("\t\e[1;35mWarning:\e[0m could not configure frequency counter\n");
      fail = 1;
      break;
    }
  }

  fclose(file);

  if (!fail) {
    devAvailable = 1;
  }

  return;
}

//==============================================================================

int frqCounter::isAvailable(void)
{
  return devAvailable;
}

//==============================================================================

void frqCounter::flush(void)
{
  char chr;;
  int count = 0;

  if (!devAvailable) {
    return;
  }

  tcflush(fileDsc, TCIFLUSH);
  // TODO: this looks nasty, but other 256 bytes of old data
  // that are buffered somewhere may arrive within next 16 ms
  usleep(TC_DELAY);
  tcflush(fileDsc, TCIFLUSH);

  do {
    if (read(fileDsc, &chr, 1) != 1) {
      printf("\t\e[1;35mWarning:\e[0m could not read"
	     " frequency data from USB device\n");
      devAvailable = 0;
      break;
    }

    if (++count > PKG_LENGTH * 2) {
      printf("\t\e[1;35mWarning:\e[0m could not flush"
	     " old frequency data from USB device\n");
      devAvailable = 0;
      break;
    }
  } while (chr != '\n');

  return;
}

//==============================================================================

double frqCounter::getFrequency(void)
{
  char str[MAX_LENGTH];
  int count;
  double frq = 0.0;

  if (!devAvailable) {
    return frq;
  }

  count = 0;

  do {
    if(read(fileDsc, str + count, 1) != 1) {
      printf("\t\e[1;35mWarning:\e[0m could not read"
	     " frequency data from USB device\n");
      devAvailable = 0;
      break;
    }
  } while (str[count++] != '\n' && count < PKG_LENGTH);

  str[count] = 0;

  if (sscanf(str, "%lf", &frq) != 1) {
    printf("\t\e[1;35mWarning:\e[0m an error occured"
	   " while trying to read frequency value\n");
  }

  return frq;
}

//==============================================================================

frqCounter::~frqCounter(void)
{
  if (fileDsc > 0) {
    tcsetattr(fileDsc, TCSANOW, &oldtio);
    close(fileDsc);
  }

  return;
}
