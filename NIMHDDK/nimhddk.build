#!/bin/bash
#
# Download & build NI MHDDK kernel module and
# XSeries driver for Debian 11.5 / Linux 5.10
#
# By DK, 2022
#

CWD=$(pwd)
KERNELVERSION=$(uname -r | cut -d "-" -f 1,2)
KERNELHEADERS=/usr/src/linux-headers-$KERNELVERSION
ARCH=$(uname -r | cut -d "-" -f 3)

NIMHDDK=nimhddk_linuxkernel
NIXSERIES=nixseries
NI_DOWNLOADS=https://download.ni.com/evaluation/labview/ekit/other/downloader

set -e
rm -rf $CWD/$NIMHDDK
rm -rf $CWD/$NIXSERIES

[ ! -f $CWD/$NIMHDDK.tar.gz ] && wget $NI_DOWNLOADS/$NIMHDDK.tar.gz
[ ! -f $CWD/$NIXSERIES.tar.gz ] && wget $NI_DOWNLOADS/$NIXSERIES.tar.gz

tar -xvf $CWD/$NIMHDDK.tar.gz
tar -xvf $CWD/$NIXSERIES.tar.gz

patch -p1 < $CWD/nimhddk_linuxkernel.patch
patch -p1 < $CWD/nixseries.patch

cd $CWD/$NIMHDDK/LinuxKernel/nirlpk
KERNELHEADERS=$KERNELHEADERS-common ./configure
sed -i "s/common/$ARCH/" Makefile.in
make

cd $CWD/$NIXSERIES
make OSINTERFACE_DIR=../$NIMHDDK OSINTERFACE_MAKEFILE=LinuxKernel.mak

cd $CWD

echo ''
echo 'NI MHDDK kernel module and XSeries driver have been sucesfully compiled'
echo -e 'Run \033[1msudo ./nimhddk.install\033[0m to install the module and init.d service'
echo ''
