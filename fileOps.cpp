// fileOps.h
//
// File operations, save and load EPR experiment
//

#include <cstdio>
#include <cstring>
#include <cerrno>

#include <archive.h>
#include <archive_entry.h>

#include "fileOps.h"
#include "supplement.h"

#define PERM_0644 (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)
#define PERM_0755 (S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH)

//==============================================================================

int saveExperiment(const char *fileName, EPR::experiment &exp)
{
  char *extPtr;
  char fnameExt[MAX_LENGTH];
  char expDsc[KB(4)] = {0};
  size_t sz;

  struct archive *arch;
  struct archive_entry *entry;

  time_t mtime = time(NULL);

  strcpy(fnameExt, fileName);

  // ignore extension if presents
  extPtr = strstr(fnameExt, ".tar.gz");
  if (extPtr == NULL) {
    extPtr = fnameExt + strlen(fnameExt);
  }

  arch = archive_write_new();
  archive_write_add_filter_gzip(arch);
  archive_write_set_format_pax_restricted(arch);

  // creating archive file
  strcpy(extPtr, ".tar.gz");

  if (archive_write_open_filename(arch, fnameExt) != ARCHIVE_OK) {
    printf("\t\e[1;35mWarning:\e[0m cannot create file \"%s\"\n", fnameExt);
    archive_write_free(arch);
    return 0;
  }

  entry = archive_entry_new();

  // experiment description
  strcpy(extPtr, ".exp");
  sz = genDescription(expDsc, exp);

  archive_entry_set_pathname(entry, fnameExt);
  archive_entry_set_size(entry, sz);
  archive_entry_set_filetype(entry, AE_IFREG);
  archive_entry_set_perm(entry, PERM_0644);
  archive_entry_set_mtime(entry, mtime, 0);

  archive_write_header(arch, entry);
  archive_write_data(arch, expDsc, sz);

  archive_entry_clear(entry);

  // xyz gradients
  strcpy(extPtr, ".xyz");
  sz = exp.xyzGauss1cm.size() * sizeof(double);

  archive_entry_set_pathname(entry, fnameExt);
  archive_entry_set_size(entry, sz);
  archive_entry_set_filetype(entry, AE_IFREG);
  archive_entry_set_perm(entry, PERM_0644);
  archive_entry_set_mtime(entry, mtime, 0);

  archive_write_header(arch, entry);
  archive_write_data(arch, exp.xyzGauss1cm.data(), sz);

  archive_entry_clear(entry);

  // epr projections
  strcpy(extPtr, ".prj");
  sz = exp.prj0.size() * sizeof(double);

  archive_entry_set_pathname(entry, fnameExt);
  archive_entry_set_size(entry, sz);
  archive_entry_set_filetype(entry, AE_IFREG);
  archive_entry_set_perm(entry, PERM_0644);
  archive_entry_set_mtime(entry, mtime, 0);

  archive_write_header(arch, entry);
  archive_write_data(arch, exp.prj0.data(), sz);

  archive_entry_clear(entry);

  // frequency data
  strcpy(extPtr, ".frq");
  sz = exp.frq.size() * sizeof(double);

  archive_entry_set_pathname(entry, fnameExt);
  archive_entry_set_size(entry, sz);
  archive_entry_set_filetype(entry, AE_IFREG);
  archive_entry_set_perm(entry, PERM_0644);
  archive_entry_set_mtime(entry, mtime, 0);

  archive_write_header(arch, entry);
  archive_write_data(arch, exp.frq.data(), sz);

  archive_entry_free(entry);

  archive_write_close(arch);
  archive_write_free(arch);

  return 1;
}

//==============================================================================
# define EPR_ARCH_OK 0b00000000000001100010100101010110

int loadExperiment(const char *fileName, EPR::experiment &exp)
{
  struct archive *arch;
  struct archive_entry *entry;

  const char *fnameExt;
  char expDsc[KB(4)];
  size_t sz, dscSz;

  FILE *file;
  char str[MAX_LENGTH];

  struct tm tmInfo;
  time_t start, end;

  EPR::settings sets;
  std::vector<double> xyz;
  std::vector<double> prj0;
  std::vector<double> frq;

  unsigned int chkCounter = 0;

  arch = archive_read_new();
  archive_read_support_filter_gzip(arch);
  archive_read_support_format_tar(arch);

  if (archive_read_open_filename(arch, fileName, MB(1)) != ARCHIVE_OK) {
    printf("\t\e[1;35mWarning:\e[0m cannot open file \"%s\"\n", fileName);
    archive_read_free(arch);
    return 0;
  }

  while (archive_read_next_header(arch, &entry) == ARCHIVE_OK) {
    fnameExt = archive_entry_pathname(entry);
    sz = archive_entry_size(entry);

    if (strstr(fnameExt, ".exp")) {
      dscSz = sz;
      archive_read_data(arch, expDsc, sz);
      chkCounter |= 1 << 1;
    }
    else if (strstr(fnameExt, ".xyz")) {
      xyz.resize(sz / sizeof(double));
      archive_read_data(arch, xyz.data(), sz);
      chkCounter |= 1 << 4;
    }
    else if (strstr(fnameExt, ".prj")) {
      prj0.resize(sz / sizeof(double));
      archive_read_data(arch, prj0.data(), sz);
      chkCounter |= 1 << 6;
    }
    else if (strstr(fnameExt, ".frq")) {
      frq.resize(sz / sizeof(double));
      archive_read_data(arch, frq.data(), sz);
      chkCounter |= 1 << 8;
    }
  }

  archive_read_close(arch);
  archive_read_free(arch);

  file = fmemopen(expDsc, dscSz, "r");
  if (loadImagingSettings(file, sets)) {
    chkCounter |= 1 << 2;
  }

  rewind(file);

  while (fgets(str, MAX_LENGTH, file)) {
    if (!strncmp(str, "# Acquisition started", 21) &&
	strptime(str + 21, " %a %b %d %H:%M:%S %Y", &tmInfo)) {
      start = mktime(&tmInfo);
      chkCounter |= 1 << 17;
    }
    else if (!strncmp(str, "# Acquisition finished", 22) &&
	     strptime(str + 22, " %a %b %d %H:%M:%S %Y", &tmInfo)) {
      end = mktime(&tmInfo);
      chkCounter |= 1 << 18;
    }
  }

  fclose(file);

  if (xyz.size() == sets.k3 * 3) {
    chkCounter |= 1 << 11;
  }

  if (prj0.size() == sets.k3 * sets.n) {
    chkCounter |= 1 << 13;
  }

  if (chkCounter == EPR_ARCH_OK) {
    exp.sets = sets;
    exp.xyzGauss1cm = xyz;
    exp.prj0 = prj0;
    exp.frq = frq;
    exp.start = start;
    exp.end = end;
  }
  else {
    printf("\t\e[1;35mWarning:\e[0m file \"%s\" is corrupted\n", fileName);
  }

  return (chkCounter == EPR_ARCH_OK);
}
//==============================================================================

void saveRawData(const char *name, EPR::experiment &exp)
{
  char *extPtr;
  char dirName[MAX_LENGTH];
  char fullName[MAX_LENGTH];
  char expDsc[KB(4)] = {0};
  size_t sz;

  FILE *file;
  int fail = 0;

  strcpy(dirName, name);

  // remove extension if presents
  extPtr = strstr(dirName, ".tar.gz");
  if (extPtr) {
    *extPtr = 0;
  }

  // create directory
  if (mkdir(dirName, PERM_0755) != 0 && errno != EEXIST) {
    printf("\t\e[1;35mWarning:\e[0m cannot create directory \"%s\"\n", dirName);
    return;
  }

  strcpy(fullName, dirName);
  strcat(fullName, "/");
  strcat(fullName, dirName);
  extPtr = fullName + strlen(fullName);

  // experiment description
  strcpy(extPtr, ".exp");
  file = fopen(fullName, "w");

  if (file != NULL) {
    sz = genDescription(expDsc, exp);
    fwrite(expDsc, sizeof(char), sz, file);
    fclose(file);
  }
  else {
    fail = 1;
  }

  // sweep profile & syncronization trigger
  strcpy(extPtr, ".sws");
  file = fopen(fullName, "w");

  if (file != NULL) {
    fwrite(exp.swDataGauss.data(), sizeof(double),
	   exp.swDataGauss.size(), file);
    fwrite(exp.syncSignal.data(), sizeof(float), exp.syncSignal.size(), file);
    fclose(file);
  }
  else {
    fail = 1;
  }

  // raw EPR data
  strcpy(extPtr, ".raw");
  file = fopen(fullName, "w");

  if (file != NULL) {
    fwrite(exp.rawData.data(), sizeof(float), exp.rawData.size(), file);
    fclose(file);
  }
  else {
    fail = 1;
  }

  // xyz gradients
  strcpy(extPtr, ".xyz");
  file = fopen(fullName, "w");

  if (file != NULL) {
    fwrite(exp.xyzGauss1cm.data(), sizeof(double),
	   exp.xyzGauss1cm.size(), file);
    fclose(file);
  }
  else {
    fail = 1;
  }

  // frequency data
  strcpy(extPtr, ".frq");
  file = fopen(fullName, "w");

  if (file != NULL) {
    fwrite(exp.frq.data(), sizeof(double), exp.frq.size(), file);
    fclose(file);
  }
  else {
    fail = 1;
  }

  if (fail) {
    strcpy(extPtr, ".*");
    printf("\t\e[1;35mWarning:\e[0m an error occured"
	   " while saving files \"%s\"\n", fullName);
  }

  return;
}

//==============================================================================

int genDescription(char *expDsc, EPR::experiment &exp)
{
  size_t sz = 0;

  sz = sprintf(expDsc, "# This file was generated automatically\n");
  sz += sprintf(expDsc + sz, "#\n");
  sz += sprintf(expDsc + sz, "# Acquisition started\t%s", ctime(&exp.start));
  sz += sprintf(expDsc + sz, "# Acquisition finished\t%s", ctime(&exp.end));
  sz += sprintf(expDsc + sz, "#\n");
  sz += sprintf(expDsc + sz, "\n");

  if (exp.sets.cfAuto && exp.sets.cfShift == 0.0) {
    sz += sprintf(expDsc + sz, "CENTER_FIELD = auto\n");
  }
  else if (exp.sets.cfAuto && exp.sets.cfShift > 0.0) {
    sz += sprintf(expDsc + sz, "CENTER_FIELD = auto + %.3f G\n",
		  exp.sets.cfShift);
  }
  else if (exp.sets.cfAuto && exp.sets.cfShift < 0.0) {
    sz += sprintf(expDsc + sz, "CENTER_FIELD = auto - %.3f G\n",
		  -exp.sets.cfShift);
  }
  else {
    sz += sprintf(expDsc + sz, "CENTER_FIELD = %.3f G\n", exp.sets.cfGauss);
  }

  sz += sprintf(expDsc + sz, "\n");
  sz += sprintf(expDsc + sz, "SWEEP_WIDTH = %.3f G\n", exp.sets.swGauss);
  sz += sprintf(expDsc + sz, "\n");
  sz += sprintf(expDsc + sz, "SCAN_TIME = %.3e s\n", exp.sets.swTime);
  sz += sprintf(expDsc + sz, "\n");
  sz += sprintf(expDsc + sz, "NUMBER_OF_POINTS = %lu\n", exp.sets.n);
  sz += sprintf(expDsc + sz, "\n");
  sz += sprintf(expDsc + sz, "NUMBER_OF_PROJECTIONS = %lu\n", exp.sets.k3);
  sz += sprintf(expDsc + sz, "\n");
  sz += sprintf(expDsc + sz, "NUMBER_OF_SCANS = %lu\n", exp.sets.nScans);
  sz += sprintf(expDsc + sz, "\n");
  sz += sprintf(expDsc + sz, "MAXIMUM_GRADIENT = %.3f G/cm\n", exp.sets.maxGr);
  sz += sprintf(expDsc + sz, "\n");

  switch (exp.sets.grDistr) {
  case EPR::gm3d:
    sz += sprintf(expDsc + sz, "GRADIENT_DISTRIBUTION = gm3d\n");
    break;
  case EPR::hcp:
    sz += sprintf(expDsc + sz, "GRADIENT_DISTRIBUTION = hcp\n");
    break;
  case EPR::spi:
    sz += sprintf(expDsc + sz, "GRADIENT_DISTRIBUTION = spi\n");
    break;
  case EPR::custom:
    sz += sprintf(expDsc + sz, "GRADIENT_DISTRIBUTION = %s\n", exp.sets.grFile);
    break;
  }

  sz += sprintf(expDsc + sz, "\n");

  switch (exp.sets.grOrder) {
  case EPR::ascend:
    sz += sprintf(expDsc + sz, "GRADIENT_ORDER = ascend\n");
    break;
  case EPR::descend:
    sz += sprintf(expDsc + sz, "GRADIENT_ORDER = descend\n");
    break;
  case EPR::spiral:
    sz += sprintf(expDsc + sz, "GRADIENT_ORDER = spiral\n");
    break;
  case EPR::random:
    sz += sprintf(expDsc + sz, "GRADIENT_ORDER = random\n");
    break;
  case EPR::snake:
    sz += sprintf(expDsc + sz, "GRADIENT_ORDER = snake\n");
    break;
  }

  sz += sprintf(expDsc + sz, "\n");
  sz += sprintf(expDsc + sz, "GRADIENT_DITHERING = %.3f\n", exp.sets.dither);

  return sz;
}
