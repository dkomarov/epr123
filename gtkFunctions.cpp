// gtkFunctions.cpp
//
// GTK-based graphical interface for epr123
//

#include <cmath>

#include <algorithm>		// std::transform
#include <functional>		// std::multiplies
#include <numeric>		// std::iota

#include <cairo.h>
#include <gtk/gtk.h>

#include "fieldSweep.h"
#include "supplement.h"
#include "gtkFunctions.h"

eprInstrument *eprInstrPtr;
std::vector<float> *rawDataPtr;
std::vector<double> *prj0Ptr;
EPR::settings *setsPtr;
EPR::status_t *statusPtr;

GtkWidget *win;
GtkWidget *drArea;

long int _prIndex;
unsigned int _isc;

// fixed set of scales for Y-axis, volts
double _yScale[] = {10.0, 5.0, 2.0, 1.0, 0.5, 0.2, 0.1};

double _x1 = 1.0 / 8.0;	// horizontal markers
double _x2 = 7.0 / 8.0;
int _xActive = 0;

double _y1 = 1.0 / 8.0;	// vertical markers
double _y2 = 7.0 / 8.0;
int _yActive = 0;

static gboolean getSpectrum(std::vector<double> *spcPtr);

static gboolean plotSpc(GtkWidget *widget, cairo_t *cr,
			std::vector<double> *spcPtr);

static gboolean tuneModeKeys(GtkWidget *win, GdkEventKey *event,
			     gpointer ignored);

static gboolean showModeKeys(GtkWidget *win, GdkEventKey *event,
			     std::vector<double> *prPtr);

static gboolean mouseButton(GtkWidget *win, GdkEventButton *event,
			    gpointer ignored);

//==============================================================================

void gTuningMode(eprInstrument &epr,
		 const EPR::settings &sets,
		 EPR::status_t &eprStatus)
{
  EPR::settings tnMode = sets;		// modified setting for tuning mode

  tnMode.k3 = 1;				// single spectrum scan
  tnMode.nCycles = tnMode.nScans / 2 + 1;	// integer division OK
  tnMode.nTrigs = tnMode.nCycles * 2;

  _prIndex = -1;

  std::vector<double> swDataGauss;
  std::vector<float> syncSignal;

  std::vector<float> rawData;
  std::vector<double> spc;

  generateFieldSweep(swDataGauss, syncSignal, tnMode);

  epr.updateConfiguration(tnMode, eprStatus);
  epr.setCenterField(tnMode.cfGauss, eprStatus);
  epr.prepAcquisition(tnMode, eprStatus);
  epr.primeFieldSweep(swDataGauss, syncSignal, eprStatus);

  if (eprStatus != EPR::idle) {
    return;
  }

  eprInstrPtr = &epr;
  rawDataPtr = &rawData;
  setsPtr = &tnMode;
  statusPtr = &eprStatus;

//==============================================================================

  guint fnId;
  gtk_init(0, NULL);

  win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_default_size(GTK_WINDOW(win), WIDTH, HEIGHT);
  gtk_window_set_title(GTK_WINDOW(win), "EPR tuning");

  drArea = gtk_drawing_area_new();
  gtk_container_add(GTK_CONTAINER(win), drArea);
  gtk_widget_add_events(drArea, GDK_BUTTON_PRESS_MASK);

  g_signal_connect(G_OBJECT(win), "destroy", gtk_main_quit, NULL);
  g_signal_connect(G_OBJECT(win), "key_press_event",
		   G_CALLBACK(tuneModeKeys), NULL);

  g_signal_connect(G_OBJECT(drArea), "draw", G_CALLBACK(plotSpc), &spc);
  g_signal_connect(G_OBJECT(drArea), "button_press_event",
		   G_CALLBACK(mouseButton), NULL);

  fnId = g_idle_add(GSourceFunc(getSpectrum), &spc);

  gtk_widget_show_all(win);
  gtk_main();

  g_source_remove(fnId);

  return;
}

//==============================================================================

void gShowData(std::vector<double> &prj0, EPR::settings &sets)
{
  if (prj0.size() != sets.n * sets.k3) {
    return;
  }

  _prIndex = 0;
  std::vector<double> pr(prj0.begin(), prj0.begin() + sets.n);

  prj0Ptr = &prj0;
  setsPtr = &sets;

  gtk_init(0, NULL);

  win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_default_size(GTK_WINDOW(win), WIDTH, HEIGHT);
  gtk_window_set_title(GTK_WINDOW(win), "EPR projections");

  drArea = gtk_drawing_area_new();
  gtk_container_add(GTK_CONTAINER(win), drArea);
  gtk_widget_add_events(drArea, GDK_BUTTON_PRESS_MASK);

  g_signal_connect(G_OBJECT(win), "destroy", gtk_main_quit, NULL);

  g_signal_connect(G_OBJECT(win), "key_press_event",
		   G_CALLBACK(showModeKeys), &pr);

  g_signal_connect(G_OBJECT(drArea), "draw", G_CALLBACK(plotSpc), &pr);

  g_signal_connect(G_OBJECT(drArea), "button_press_event",
		   G_CALLBACK(mouseButton), NULL);

  gtk_widget_show_all(win);
  gtk_main();

  return;
}

//==============================================================================

static gboolean getSpectrum(std::vector<double> *spcPtr)
{
  eprInstrPtr->retriggerableScan(*rawDataPtr, *setsPtr, *statusPtr);
  if (*statusPtr != EPR::idle) {
    return FALSE;	// FALSE to stop calling the function if error occurs
  }

  mergeProjections(*spcPtr, *rawDataPtr, *setsPtr);
  _prIndex++;

  gtk_widget_queue_draw(drArea);
  return TRUE;				// TRUE to be called repeatedly
}

//==============================================================================

static gboolean plotSpc(GtkWidget *widget, cairo_t *cr,
			std::vector<double> *spcPtr)
{
  size_t i, n;
  double sx, sy, tx, ty;
  char str[MAX_LENGTH];

  std::vector<double> &spc = *spcPtr;
  n = spc.size();

  GdkRectangle cnv;
  GdkWindow *win = gtk_widget_get_window(widget);
  gdk_window_get_geometry(win, &cnv.x, &cnv.y, &cnv.width, &cnv.height);

  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_paint(cr);

  cairo_set_source_rgb(cr, 0.33, 0.33, 0.33);
  cairo_move_to(cr, 0.0, cnv.height / 2.0);
  cairo_line_to(cr, cnv.width, cnv.height / 2.0);
  cairo_move_to(cr, cnv.width / 2.0, 0.0);
  cairo_line_to(cr, cnv.width / 2.0, cnv.height);
  cairo_set_line_width(cr, 1.5);
  cairo_stroke(cr);

  cairo_matrix_t crMx;
  cairo_get_matrix(cr, &crMx);

  sx = static_cast<double>(cnv.width) / n;
  sy = cnv.height / _yScale[_isc] / 2.0;

  cairo_translate(cr, 0.0, cnv.height / 2.0);
  cairo_scale(cr, sx, -sy);

  cairo_set_source_rgb(cr, 0.25, 1.0, 0.25);
  for (i = 0; i < n; ++i) {
    cairo_line_to(cr, i, spc[i]);
  }

  cairo_set_matrix(cr, &crMx);
  cairo_set_line_width(cr, 1.5);
  cairo_stroke(cr);

  cairo_set_source_rgb(cr, 1.0, 0.0, 0.0);
  cairo_move_to(cr, cnv.width * _x1, 0.0);
  cairo_line_to(cr, cnv.width * _x1, cnv.height);
  cairo_set_line_width(cr, _xActive ? 1.0 : 2.0);
  cairo_stroke(cr);

  cairo_move_to(cr, cnv.width * _x2, 0.0);
  cairo_line_to(cr, cnv.width * _x2, cnv.height);
  cairo_set_line_width(cr, _xActive ? 2.0 : 1.0);
  cairo_stroke(cr);

  cairo_set_source_rgb(cr, 0.0, 0.25, 1.0);
  cairo_move_to(cr, 0.0, cnv.height * _y1);
  cairo_line_to(cr, cnv.width, cnv.height * _y1);
  cairo_set_line_width(cr, _yActive ? 1.0 : 2.0);
  cairo_stroke(cr);

  cairo_move_to(cr, 0.0, cnv.height * _y2);
  cairo_line_to(cr, cnv.width, cnv.height * _y2);
  cairo_set_line_width(cr, _yActive ? 2.0 : 1.0);
  cairo_stroke(cr);

  cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
  cairo_set_font_size(cr, FONT_SZ);
  tx = cnv.width - FONT_SZ * 12.0;
  ty = FONT_SZ * 4.0;

  cairo_move_to(cr, tx, ty);
  sprintf(str, "Projection #%ld", _prIndex + 1);
  cairo_show_text(cr, str);

  ty += FONT_SZ * 1.2;
  cairo_move_to(cr, tx, ty);
  sprintf(str, "%.2lf %c%c %.2lf G",
	  setsPtr->cfGauss, 0xc2, 0xB1, setsPtr->swGauss / 2.0);
  cairo_show_text(cr, str);

  ty += FONT_SZ * 1.2;
  cairo_move_to(cr, tx, ty);
  sprintf(str, "dX =  %.3lf G", fabs(_x1 - _x2) * setsPtr->swGauss);
  cairo_show_text(cr, str);

  ty += FONT_SZ * 1.2;
  cairo_move_to(cr, tx, ty);
  sprintf(str, "dY =  %.3lf V", fabs(_y1 - _y2) * _yScale[_isc] * 2.0);
  cairo_show_text(cr, str);

  return TRUE;		// TRUE to stop other handlers from being envoked
}

//==============================================================================

static gboolean tuneModeKeys(GtkWidget *widget,
			     GdkEventKey *event, gpointer unused)
{
  if (event->keyval == GDK_KEY_Escape) {
    gtk_window_close(GTK_WINDOW(win));
    return TRUE;
  }
  else if (event->keyval == GDK_KEY_Up && _isc < 6) {
    _isc++;
  }
  else if (event->keyval == GDK_KEY_Down && _isc > 0) {
    _isc--;
  }
  else if(event->keyval == GDK_KEY_Tab && event->state & GDK_CONTROL_MASK) {
    _yActive = !(_yActive);
  }
  else if(event->keyval == GDK_KEY_Tab) {
    _xActive = !(_xActive);
  }

  gtk_widget_queue_draw(drArea);

  return TRUE;		// TRUE to stop other handlers from being envoked
}

//==============================================================================

static gboolean showModeKeys(GtkWidget *widget, GdkEventKey *event,
			     std::vector<double> *prPtr)
{
  if (event->keyval == GDK_KEY_Escape) {
    gtk_window_close(GTK_WINDOW(win));
    return TRUE;
  }
  else if (event->keyval == GDK_KEY_Up && _isc < 6) {
    _isc++;
  }
  else if (event->keyval == GDK_KEY_Down && _isc > 0) {
    _isc--;
  }
  else if (event->keyval == GDK_KEY_Left && event->state & GDK_SHIFT_MASK) {
    _prIndex -= 10;
  }
  else if (event->keyval == GDK_KEY_Right && event->state & GDK_SHIFT_MASK) {
    _prIndex += 10;
  }
  else if (event->keyval == GDK_KEY_Left && event->state & GDK_CONTROL_MASK) {
    _prIndex -= 100;
  }
  else if (event->keyval == GDK_KEY_Right && event->state & GDK_CONTROL_MASK) {
    _prIndex += 100;
  }
  else if (event->keyval == GDK_KEY_Left) {
    _prIndex -= 1;
  }
  else if (event->keyval == GDK_KEY_Right) {
    _prIndex += 1;
  }
  else if (event->keyval == GDK_KEY_Home) {
    _prIndex = 0;
  }
  else if (event->keyval == GDK_KEY_End) {
    _prIndex = setsPtr->k3 * setsPtr->nScans - 1;
  }
  else if(event->keyval == GDK_KEY_Tab && event->state & GDK_CONTROL_MASK) {
    _yActive = !(_yActive);
  }
  else if(event->keyval == GDK_KEY_Tab) {
    _xActive = !(_xActive);
  }

  if (_prIndex < 0) {
    _prIndex = 0;
  }
  else if (static_cast<long unsigned int>(_prIndex) > setsPtr->k3 - 1) {
    _prIndex = setsPtr->k3 - 1;
  }

  std::copy_n(prj0Ptr->begin() + setsPtr->n * _prIndex,
	      setsPtr->n, prPtr->begin());

  gtk_widget_queue_draw(drArea);

  return TRUE;		// TRUE to stop other handlers from being envoked
}

//==============================================================================

static gboolean mouseButton(GtkWidget *widget,
			    GdkEventButton *event, gpointer ignored)
{
  if (event->button != 1) {
    return TRUE;
  }

  GdkRectangle cnv;
  GdkWindow *win = gtk_widget_get_window(widget);
  gdk_window_get_geometry(win, &cnv.x, &cnv.y, &cnv.width, &cnv.height);

  if (event->state & GDK_CONTROL_MASK) {
    if (_yActive) {
      _y2 = event->y / cnv.height;
    }
    else {
      _y1 = event->y / cnv.height;
    }
  }
  else {
    if (_xActive) {
      _x2 = event->x / cnv.width;
    }
    else {
      _x1 = event->x / cnv.width;
    }
  }

  gtk_widget_queue_draw(drArea);

  return TRUE;		// TRUE to stop other handlers from being envoked
}
